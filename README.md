# Custom Player #

Custom Player ist ein Videoplayer, dessen Aussehen und Funktionalität selbst gestaltet werden kann. Es besteht die Auswahl in der Positionierung von Controlbar und Playlist, bei der farblichen Gestaltung und Aussehen der Buttons. Des Weiteren können eigene Hotkeys für diverse Funktionen belegt werden. Es können Links von diversen Seiten, wie z.B Youtube, zur Playlist hinzugefügt und abgespielt werden. 


## Setup ##

Zur Installation von *Custom Player* wird eine aktuelle Version von Node.js benötigt.
Diese können sie [hier](https://nodejs.org/en/) herunterladen.
Nach abgeschlossener Installation von Node.js muss dieses Git Repository gecloned werden.

Ebenso muss Youtube-dl und MongoDB installiert sein.

Dazu muss folgender Befehl in einem Commandlinetool wie GitBash aufgerufen werden.


```

git clone https://git@bitbucket.org/mmess16projekte/customplayer.git
```


Anschließend müssen im Ordner *customplayer* einige Packete installiert werden:


```
cd customplayer
npm install

```

Nun muss eine Datenbank geöffnet werden.


```
mongod

```


Öffnen sie nun ein neues GitBash, um den Server zu starten.


```
grunt

```

Rufen sie nun [http://localhost:3000](http://localhost:3000) auf, um den *Custom Player* zu öffnen.