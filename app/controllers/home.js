/* eslint-env node */
"use strict";

var express = require("express"),
    router = express.Router(),
    mongoose = require("mongoose"),
    Settings = mongoose.model("Settings"),
    exec = require("child_process").exec;

module.exports = function (app) {
    app.use("/", router);
};

router.get("/", function (req, res, next) {
    // TODO find better functions (all names, one specific setting)
    Settings.find(function (err, settings) {
        var i,
            themeName = "Standardtheme",
            currentSettings,
            themeNames = [];

        if(err) {
            return next(err);
        }

        for ( i = 0; i < settings.length; i++) {
            if (themeName === settings[i].themeName) {
                currentSettings = settings[i];
            }
            themeNames.push(settings[i].themeName);
        }
        return res.render("index", {
            title: "CustomPlayer",
            settings:  JSON.stringify(currentSettings),
            themeNames: themeNames
        });
    });
})
.get("/ytdl", function (req, res, next) {
    exec("youtube-dl -j " + req.query.q, function(error, stdout, stderr) {
        if(error) {
            return next(error);
        }
        if(stderr) {
            return next(stderr);
        }

        if(stdout) {
            return res.send(stdout);
        }
        return next();
    });
})
.get("/t/", function (req, res, next) {
    Settings.findOne({ themeName: req.query.q }, function(err, settings) {
        if(err) {
            return next(err);
        }

        return res.send(settings);
    });
})
.post("/save", function(req, res, next) {
    var newSettings = req.body;

    Settings.find({ themeName: newSettings.themeName }, function (err, settings) {
        if (err) {
            return next(err);
        }
        if (settings.length !== 0) {
            return next();
        }
        new Settings(newSettings).save(function (err) {
            if (err) {
                return next(err);
            }
            return res.status(200);
        });
        return res.sendStatus(200);
    });
})
.get("/:themeName", function(req, res, next) {
    // TODO find better functions (all names, one specific setting)
    Settings.find(function (err, settings) {
        var i,
            currentSettings,
            themeNames = [];

        if(err) {
            return next(err);
        }

        for ( i = 0; i < settings.length; i++) {
            if (req.params.themeName === settings[i].themeName) {
                currentSettings = settings[i];
            }
            themeNames.push(settings[i].themeName);
        }
        if (!currentSettings) {
            return next();
        }

        return res.render("index", {
            title: currentSettings.themeName + " - CustomPlayer",
            settings:  JSON.stringify(currentSettings),
            themeNames: themeNames
        });
    });
});
//.put("/save", function(req, res) {
    ////console.log("Update/PUT");
    ////res.json(req.body);
    ////console.log(req.body);
    ////saveSettings(req.body);
//})
