/* eslint-env node */
"use strict";

var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    SettingsSchema,
    SettingsModel,
    standardSetting;

SettingsSchema = new Schema({
    themeName: String,
    features: {
        playPauseButton: { type: Number, default: 1 }, // corresponds to controllbar-position
        seekBar: { type: Number, default: 2 }, // corresponds to controllbar-position (0 = hidden)
        stopButton: { type: Number, default: 3 }, // corresponds to controllbar-position (0 = hidden)
        volume: { type: Number, default: 4 }, // corresponds to controllbar-position (0 = hidden)
        repeatButton: { type: Number, default: 5 }, // corresponds to controllbar-position (0 = hidden)
        prevNextButtons: { type: Number, default: 6 }, // corresponds to controllbar-position (0 = hidden)
        fullscreenButton: { type: Number, default: 7 }, // corresponds to controllbar-position (0 = hidden)
        popOutButton: { type: Number, default: 8 }, // corresponds to controllbar-position (0 = hidden)
        theaterMode: { type: Number, default: 9 }, // corresponds to controllbar-position (0 = hidden)
        addToPlaylist: { type : Number, default: 10 }, // corresponds to controllbar-position
        optionsButton: { type : Number, default: 11 }, // corresponds to controllbar-position
        controllBarPosition: { type: Number, default: 3 } // corresponds to position of controllbar (top/bottom/overlay/hidden)
    },
    style: {
        backgroundColor: String, // hex-value
        highlightColor: String, // hex-value
        buttonColor: String,
        backgroundPic: String, // URL/URI
        playerDefaultPic: String, // URL/URI
        buttonStyle: String, // name of button style
        customCSS: String // URL/URI
    },
    hotkeys: {
        playPause: String, // Mousetrap key string
        seekForward: String, // Mousetrap key string
        seekBackward: String, // Mousetrap key string
        jumpForward: String, // Mousetrap key string
        jumpBackward: String, // Mousetrap key string
        stop: String, // Mousetrap key string
        volumeUp: String, // Mousetrap key string
        volumeDown: String, // Mousetrap key string
        mute: String, // Mousetrap key string
        repeat: String, // Mousetrap key string
        prevTrack: String, // Mousetrap key string
        nextTrack: String, // Mousetrap key string
        fullscreen: String, // Mousetrap key string
        popOut: String, // Mousetrap key string
        theaterMode: String, // Mousetrap key string
        addToPlaylist: String, // Mousetrap key string
        options: String // Mousetrap key string
    },
    playlist: {
        position: Number, // corresponds to position of playlist (left/right/etc.)
        displayDuration: Boolean, // show/hide Button
        displayLink: Boolean, // show/hide Button
        controlbar: Boolean, // show/hide Button
        addToPlaylistButton: Boolean, // show/hide Button
        autoplayPlaylistButton: Boolean, // show/hide Button
        prevNextButtons: Boolean, // show/hide Button
        repeatButton: Boolean, // show/hide Button
        shuffleButton: Boolean, // show/hide Button
        reverseOrderButton: Boolean // show/hide Button
    },
    player: {
        numberOfPlayers: { type: Number, min: 1, max: 4 },
        width: { type: Number, min: 10, max: 100 }, // in %
        height: { type: Number, min: 10, max: 100 }, // in %
        relativePosition: Number // corresponds to position (center/left/right/top/bottom)
    },
    date: { type: Date, default: Date.now }
});

standardSetting = {
    themeName: "Standardtheme",
    features: {
        playPauseButton: 1,
        seekBar: 2,
        stopButton: 3,
        volume: 4,
        repeatButton: 5,
        prevNextButtons: 6,
        fullscreenButton: 7,
        popOutButton: 8,
        theaterMode: 9,
        addToPlaylist: 10,
        optionsButton: 11,
        controllBarPosition: 3 
    },
    style: {
        backgroundColor: "FFFFFF",
        highlightColor: "000000",
        buttonColor: "AAAAAA",
        backgroundPic: "myurl.com/backgroundpic.png",
        playerDefaultPic: "myurl.com/playerpic.jpg",
        buttonStyle: "Numix",
        customCSS: "myurl.com/custom.css"
    },
    hotkeys: {
        playPause: "space",
        seekForward: "w",
        seekBackward: "b",
        jumpForward: "right",
        jumpBackward: "left",
        stop: "s",
        volumeUp: "up",
        volumeDown: "down",
        mute: "m",
        repeat: "r",
        prevTrack: "p",
        nextTrack: "n",
        fullscreen: "f",
        popOut: "u",
        addToPlaylist: "a",
        theaterMode: "t",
        options: "o"
    },
    playlist: {
        position: 1,
        displayDuration: true,
        displayLink: true,
        controlbar: true,
        addToPlaylistButton: true,
        autoplayPlaylistButton: true,
        prevNextButtons: true,
        repeatButton: true,
        shuffleButton: true,
        reverseOrderButton: true
    },
    player: {
        numberOfPlayers: 1,
        width: 100,
        height: 100,
        relativePosition: 1
    }
};

SettingsModel = mongoose.model("Settings", SettingsSchema);
SettingsModel.findOne({ themeName: standardSetting.themeName },
        function (err, setting) {
            if (!setting) {
                new SettingsModel(standardSetting).save();
            }
        });
