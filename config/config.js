/* eslint-env node */
"use strict";

var path = require("path"),
    rootPath = path.normalize(__dirname + "/.."),
    env = process.env.NODE_ENV || "development";

var config = {
    development: {
        root: rootPath,
        app: {
            name: "customplayer"
        },
        port: process.env.PORT || 3000,
        db: "mongodb://localhost/customplayer-development"
    },

    test: {
        root: rootPath,
        app: {
            name: "customplayer"
        },
        port: process.env.PORT || 3000,
        db: "mongodb://localhost/customplayer-test"
    },

    production: {
        root: rootPath,
        app: {
            name: "customplayer"
        },
        port: process.env.PORT || 3000,
        db: "mongodb://localhost/customplayer-production"
    }
};

module.exports = config[env];
