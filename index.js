/* eslint-env node */
"use strict";
var myModule = (function() {
    var that = {},
        express = require("express"),
        app = express(),
        exec = require("child_process").exec,
        bodyParser = require("body-parser"),
        mongoose = require("mongoose"),
        connection,
        Schema,
        settingsSchema,
        settingsModel;

    function run() {
        initServer();
        initMongoDB();
        startServer();
    }

    function initMongoDB() {
        connection = mongoose.connect("mongodb://localhost/local"),
        Schema = mongoose.Schema,
        settingsSchema = new Schema({
            themeName : String,
            features : {
                playPauseButton : { type : Number, default : 1 }, // corresponds to controllbar-position
                seekBar : Number, // corresponds to controllbar-position (0 = hidden)
                stopButton : Number, // corresponds to controllbar-position (0 = hidden)
                volume : Number, // corresponds to controllbar-position (0 = hidden)
                repeatButton : Number, // corresponds to controllbar-position (0 = hidden)
                prevNextButtons : Number, // corresponds to controllbar-position (0 = hidden)
                fullscreenButton : Number, // corresponds to controllbar-position (0 = hidden)
                popOutButton : Number, // corresponds to controllbar-position (0 = hidden)
                theaterMode : Number, // corresponds to controllbar-position (0 = hidden)
                addToPlaylist : { type : Number, default: 10 }, // corresponds to controllbar-position
                optionsButton : { type : Number, default: 11 }, // corresponds to controllbar-position
                controllBarPosition : Number // corresponds to position of controllbar (top/bottom/overlay/hidden)
            },
            style : {
                backgroundColor : String, // hex-value
                highlightColor : String, // hex-value
                backgroundPic : String, // URL/URI
                playerDefaultPic : String, // URL/URI
                buttonStyle : Number, // corresponds to style
                customCSS : String // URL/URI
            },
            hotkeys : {
                playPause : Number, // keyCode
                seekForward : Number, // keyCode
                seekBackward : Number, // keyCode
                jumpForward : Number, // keyCode
                jumpBackward : Number, // keyCode
                stop : Number, // keyCode
                volumeUp : Number, // keyCode
                volumeDown : Number, // keyCode
                mute : Number, // keyCode
                repeat : Number, // keyCode
                prevTrack : Number, // keyCode
                nextTrack : Number, // keyCode
                fullscreen : Number, // keyCode
                popOut : Number, // keyCode
                theaterMode : Number, // keyCode
                addToPlaylist : Number, // keyCode
                options : Number // keyCode
            },
            playlist : {
                position : Number, // corresponds to position of playlist (left/right/etc.)
                displayDuration : Boolean, // show/hide Button
                displayLink : Boolean, // show/hide Button
                repeatButton : Boolean, // show/hide Button
                shuffleButton : Boolean, // show/hide Button
                prevNextButtons : Boolean, // show/hide Button
                reverseOrderButton : Boolean // show/hide Button
            },
            player : {
                numberOfPlayers : { type : Number, min : 1, max : 4 },
                width : { type : Number, min : 10, max : 100 }, // in %
                height : { type : Number, min : 10, max : 100 }, // in %
                relativePosition : Number // corresponds to position (center/left/right/top/bottom)
            },
            date: { type: Date, default: Date.now }
        });
        settingsModel = mongoose.model("Settings", settingsSchema);
    }

    function saveSettings(settings) {
        var tmpModel,
            promise = getSettings(settings.themeName);
        
        promise.then(function(doc) {
            if(doc.length === 0) {
                tmpModel = new settingsModel(settings);
                tmpModel.save(function(error) {
                    if(error) {
                        // errorhandling
                        console.log(error);
                    } else {
                        console.log("New Theme saved: " + tmpModel.themeName);
                    }
                });
            } else {
                console.log("Theme already in DB, not saved!");
            }
        });
    }

    function getSettings(themeName) {
        return settingsModel.find({ themeName: themeName }).exec();
    }

    function initServer() {
        app.use(express.static(__dirname + "/static"));
        //app.use(bodyParser.urlencoded({ extended : false }));
        app.use(bodyParser.json());
        app.get("/ytdl", function (req, res) {
            exec("youtube-dl -j " + req.query.q, function(error, stdout, stderr) {
                if(error) {
                    console.log(error);
                    res.sendStatus(500);
                    return;
                }
                if(stderr) {
                    console.log(stderr);
                    res.sendStatus(500);
                    return;
                }
                if(stdout) {
                    res.send(stdout);
                }
            });
        });
        // TODO ajax/api calls & template calls
        //app.get("/:themeName", function(req, res) {
            ////template jade
        //});
        app.get("/t/:themeName", function(req, res) {
            // call mongoDB for theme css.
            // res.send(themeCSS);
            // res.sendFile(themeCSS);
            // res.sendFile("custom.css", { root: __dirname + "/static/res/css" });
            var promise = getSettings(req.params.themeName);
            promise.then(function(doc) {
                res.send(doc[0]);
            })
            .catch(function(e, xhr, response) {
                // errorhandling
                res.sendStatus(500);
            });
        });
        app.post("/save", function(req, res) {
            saveSettings(req.body);
            //res.sendStatus(200);
        });
        app.put("/save", function(req, res) {
            console.log("PUT");
            //res.send(req.body);
            //res.json(req.body);
            //console.log(req.body);
            //saveSettings(req.body);
        });
    }

    function startServer() {
        app.listen(3000, function() {
            console.log("Express server running on 3000...");
        });
    }

    that.run = run;
    return that;
}());

myModule.run();
