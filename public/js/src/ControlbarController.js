var App = App || {};
App.ControlbarController = function (options) {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher, window */
    
    var that = new EventPublisher(),
        controlbars = options.controlbars;

    function addControlbarEvent(ctrlBar) {
        ctrlBar.addEventListener("input", function (e) {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (target.classList.contains("seekbar")) {
                that.notifyAll("seekbarChanged", target.value);
            }
        });
        ctrlBar.addEventListener("change", function (e) {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (target.classList.contains("seekbar")) {
                that.notifyAll("seekbarRelease");
            }
            if (target.classList.contains("volume")) {
                that.notifyAll("volumeChanged", Number(target.value));
            }
        });
        ctrlBar.addEventListener("click", function (e) {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (target.classList.contains("play") || target.classList.contains("pause")) {
                that.notifyAll("playPressed");
            }
            if (target.classList.contains("fullscreen")) {
                that.notifyAll("fullscreenPressed");
            }
            if (target.classList.contains("options")) {
                that.notifyAll("optionsPressed");
            }
            if (target.classList.contains("stop")) {
                that.notifyAll("stopPressed");
            }
            if (target.classList.contains("repeat") || target.classList.contains("repeatActive")) {
                that.notifyAll("repeatPressed");
            }
            if (target.classList.contains("prev")) {
                that.notifyAll("prevPressed");
            }
            if (target.classList.contains("next")) {
                that.notifyAll("nextPressed");
            }
            if (target.classList.contains("theaterMode")) {
                that.notifyAll("theatermodePressed");
            }
            if (target.classList.contains("addToPlaylist")) {
                that.notifyAll("addToPlaylistPressed");
            }
        });
    }
    
    function init() {
        var i;
     
        for (i = 0; i < controlbars.length; i++) {
            addControlbarEvent(controlbars[i]);
        }
    }
    
    that.addControlbarEvent = addControlbarEvent;
    that.init = init;
    return that;
};
