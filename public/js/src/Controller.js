var App = App || {};
App.Controller = function (options) {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, Mousetrap */
    
    var that = new EventPublisher(),
        promptContainer = options.promptContainer,
        playlistControlbar = options.playlistControlbar,
        playlistEntries = options.playlistEntries,
        tablinks = options.optionTabs,
        backgroundColor = options.backgroundColor,
        buttonColor = options.buttonColor,
        highlightColor = options.highlightColor,
        playPauseSelect = options.playPauseSelect,
        seekBarSelect = options.seekBarSelect,
        stopSelect = options.stopSelect,
        volumeSelect = options.volumeSelect,
        repeatSelect = options.repeatSelect,
        prevNextSelect = options.prevNextSelect,
        fullscreenSelect = options.fullscreenSelect,
        popOutSelect = options.popOutSelect,
        lightSwitchSelect = options.lightSwitchSelect,
        addToPlaylistSelect = options.addToPlaylistSelect,
        optionsSelect = options.optionsSelect,
        buttonStyleSelect = options.buttonStyleSelect,
        backgroundImageSelect = options.backgroundImageSelect,
        themeSelect = options.themeSelect,
        themeSaveButton = options.themeSaveButton,
        controlbarSelect = options.controlbarSelect,
        playlistSelect = options.playlistSelect,
        playlistTab = options.playlistTab,
        hotkeyTab = options.hotkeyTab;
     
    function initTabs() {
        var i;
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].addEventListener("click", function (e) {
                var target;
                e = e || window.event;
                target = e.target || e.srcElement;
                if (target.parentElement.classList.contains("featureTab")) {
                    that.notifyAll("featureTabPressed");
                }
                if (target.parentElement.classList.contains("styleTab")) {
                    that.notifyAll("styleTabPressed");
                }
                if (target.parentElement.classList.contains("hotkeyTab")) {
                    that.notifyAll("hotkeyTabPressed");
                }
                if (target.parentElement.classList.contains("playlistTab")) {
                    that.notifyAll("playlistTabPressed");
                }
            });
        }
    }
    
    function onFullscreenExit(event) {
        if (document) {
            if (event.keyCode === 27) {
                that.notifyAll("escPressed");
            }
        }
    }
    
    function initStyleTab() {
        highlightColor.onchange = function () {
            that.notifyAll("selectedHighlightColor", highlightColor.value);
        };
        backgroundColor.onchange = function () {
            that.notifyAll("selectedBackgroundColor", backgroundColor.value);
        };
        buttonColor.onchange = function () {
            that.notifyAll("selectedButtonColor", buttonColor.value);
        };
        buttonStyleSelect.onchange = function () {
            that.notifyAll("selectedButtonStyle", buttonStyleSelect.value);
        };
        themeSelect.onchange = function () {
            that.notifyAll("selectedTheme", themeSelect.value);
        };
        themeSaveButton.addEventListener("click", function () {
            that.notifyAll("saveThemePressed");
        });
        backgroundImageSelect.onchange = function () {
            that.notifyAll("backgroundImageSelected", backgroundImageSelect.files[0].name);            
        };
    }
    
    function initFeatureTab() {
        playPauseSelect.onchange = function () {
            that.notifyAll("selectedPlayPausePosition", playPauseSelect.value);
        };
        seekBarSelect.onchange = function () {
            that.notifyAll("selectedSeekBarPosition", seekBarSelect.value);
        };
        stopSelect.onchange = function () {
            that.notifyAll("selectedStopPosition", stopSelect.value);
        };
        volumeSelect.onchange = function () {
            that.notifyAll("selectedVolumePosition", volumeSelect.value);
        };
        repeatSelect.onchange = function () {
            that.notifyAll("selectedRepeatPosition", repeatSelect.value);
        };
        prevNextSelect.onchange = function () {
            that.notifyAll("selectedPrevNextPosition", prevNextSelect.value);
        };
        fullscreenSelect.onchange = function () {
            that.notifyAll("selectedFullscreenPosition", fullscreenSelect.value);
        };
        popOutSelect.onchange = function () {
            that.notifyAll("selectedPopOutPosition", popOutSelect.value);
        };
        lightSwitchSelect.onchange = function () {
            that.notifyAll("selectedLightSwitchPosition", lightSwitchSelect.value);
        };
        addToPlaylistSelect.onchange = function () {
            that.notifyAll("selectedAddToPlaylistPosition", addToPlaylistSelect.value);
        };
        optionsSelect.onchange = function () {
            that.notifyAll("selectedOptionsPosition", optionsSelect.value);
        };
        controlbarSelect.onchange = function () {
            that.notifyAll("selectedControllbarPosition", controlbarSelect.value);
        };
    }

    function onPlaylistSettingsChanged(event) {
        switch (event.target.id) {
        case "show-playlist-controlbar-option":
            that.notifyAll("changedPlaylistControlbarOption", event.target.checked);
            break;
        case "add-to-playlist-option":
            that.notifyAll("changedAddToPlaylistOption", event.target.checked);
            break;
        case "autoplay-playlist-option":
            that.notifyAll("changedAutoplayPlaylistOption", event.target.checked);
            break;
        case "prev-next-option":
            that.notifyAll("changedPrevNextOption", event.target.checked);
            break;
        case "repeat-option":
            that.notifyAll("changedRepeatOption", event.target.checked);
            break;
        case "shuffle-option":
            that.notifyAll("changedShuffleOption", event.target.checked);
            break;
        case "reverse-option":
            that.notifyAll("changedReverseOption", event.target.checked);
            break;
        default:
            break;
        }
    }
        
    function initPlaylistTab() {
        var children = playlistTab.children,
            i;
        playlistSelect.onchange = function () {
            that.notifyAll("selectedPlaylistPosition", playlistSelect.value);
        };
        for (i = 0; i < children.length; i++) {
            if (children[i].querySelector("input")) {
                children[i].querySelector("input").addEventListener("change", onPlaylistSettingsChanged);
            }
        }
    }
    
    function initHotkeyTab() {
        hotkeyTab.addEventListener("click", function (event) {
            if (event.target.tagName === "BUTTON") {
                event.target.blur();
                that.notifyAll("clickedHotkeyButton", event.target);
            }
        });
    }

    function onPlaylistControlsClicked(event) {
        var target = event.target;
        while (target.tagName !== "BUTTON") {
            if (target === playlistControlbar) {
                return;
            }
            target = target.parentElement;
        }
        switch (target.id) {
        case "add-to-playlist-button-pl":
            that.notifyAll("addToPlaylistPressed");
            break;
        case "autoplay-playlist-button-pl":
            that.notifyAll("autoplayPlaylistPressed");
            break;
        case "prev-button-pl":
            that.notifyAll("prevPressed");
            break;
        case "next-button-pl":
            that.notifyAll("nextPressed");
            break;
        case "repeat-playlist-button-pl":
            that.notifyAll("repeatPlaylistPressed");
            break;
        case "shuffle-button-pl":
            that.notifyAll("shufflePressed");
            break;
        case "reverse-button-pl":
            that.notifyAll("reversePressed");
            break;
        default:
            break;
        }
    }
    
    
    function onPlaylistEntryClicked(event) {
        var target = event.target,
            playlistPosition;
        
        if(target.classList.contains("deleteButton")) {
            playlistPosition = parseInt(target.parentElement.attributes.position.value);
            that.notifyAll("clickedPlaylistEntryDelete", playlistPosition);
            return;
        }
        if (target === playlistEntries) {
            return;
        }
        while (!target.classList.contains("entry")) {
            target = target.parentElement;
        }
        playlistPosition = parseInt(target.attributes.position.value);
        that.notifyAll("clickedPlaylistEntry", playlistPosition);
        return;
    }
    
    function parseURLInput(input) {
        var urls = [],
            i;

        urls = input.split(",");
        for (i = 0; i < urls.length; i++) {
            urls[i] = urls[i].trim();
            that.notifyAll("urlEntered", urls[i]);
        }
    }

    function initPlaylist() {
        playlistEntries.addEventListener("click", onPlaylistEntryClicked);
        playlistControlbar.addEventListener("click", onPlaylistControlsClicked);
    }

    function onPromptClick(event) {
        if (event.target === promptContainer ||
                event.target === promptContainer.querySelector("#close-prompt")) {
            that.notifyAll("closePrompt");
        }
    }

    function onPromptSubmit(event) {
        var target = event.target;
        while (!target.classList.contains("promptItem")) {
            target = target.parentElement;
        }
        if (target.classList.contains("addLink")) {
            parseURLInput(event.target[0].value);
            that.notifyAll("closePrompt");
        }
        if (target.classList.contains("saveTheme")
                && event.target.children[0].value !== ""
                && !event.target.children[0].validity.patternMismatch) {
            that.notifyAll("confirmTheme", {
                themeName: event.target.children[0].value,
                isSubmitted: true
            });
        }
    }

    function onPromptKeyUp(event) {
        var target = event.target;
        if (event.key === "Escape" || event.keycode === 27) {
            that.notifyAll("closePrompt");
            return;
        }

        while (!target.classList.contains("promptItem")) {
            target = target.parentElement;
        }
        if (target.classList.contains("saveTheme")
                && event.target.value !== ""
                && !event.target.validity.patternMismatch) {
            that.notifyAll("confirmTheme", {
                themeName: event.target.value,
                isSubmitted: false
            });
        }
    }

    function initPromptContainer() {
        promptContainer.addEventListener("click", onPromptClick);
        promptContainer.addEventListener("submit", onPromptSubmit);
        promptContainer.addEventListener("keyup", onPromptKeyUp);
    }

    function init() {
        document.addEventListener("keyup", onFullscreenExit);
        Mousetrap.reset();
        initTabs();
        initStyleTab();
        initPlaylist();
        initFeatureTab();
        initPlaylistTab();
        initHotkeyTab();
        initPromptContainer();
    }

    that.init = init;
    return that;
};
