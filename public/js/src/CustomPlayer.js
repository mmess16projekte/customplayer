var App = App || {};
App.CustomPlayer = (function () {
    "use strict";
    /* eslint-env browser */
    /* global SVGInjector */
    
    var that = {},
        controller,
        view,
        model,
        controlbarController,
        video,
        hotkeys,
        server,
        initialSettings;
    
    function setUp() {
        controller = new App.Controller({
            promptContainer : document.querySelector(".promptContainer"),
            playlistControlbar : document.querySelector(".playlistControlbar"),
            playlistEntries : document.querySelector(".playlistEntries"),
            optionTabs : document.querySelectorAll(".tablinks"),
            backgroundColor : document.querySelector(".backgroundColor"),
            highlightColor : document.querySelector(".highlightColor"),
            buttonColor: document.querySelector(".buttonColor"),
            playPauseSelect : document.querySelector("#featurePlay"),
            seekBarSelect : document.querySelector("#featureSeekbar"),
            stopSelect : document.querySelector("#featureStop"),
            volumeSelect : document.querySelector("#featureVolume"),
            repeatSelect : document.querySelector("#featureRepeat"),
            prevNextSelect : document.querySelector("#featurePrevNext"),
            fullscreenSelect : document.querySelector("#featureFullscreen"),
            popOutSelect : document.querySelector("#featurePopOut"),
            lightSwitchSelect : document.querySelector("#featureLightSwitch"),
            addToPlaylistSelect : document.querySelector("#featureAddToPlaylist"),
            optionsSelect : document.querySelector("#featureOptionsFeature"),
            buttonStyleSelect : document.querySelector("#buttonStyle"),
            backgroundImageSelect : document.querySelector(".imageUpload"),
            themeSelect : document.querySelector("#themeSelect"),
            themeSaveButton : document.querySelector("#themeSaveButton"),
            controlbarSelect : document.querySelector("#featureControllBarPosition"),
            playlistSelect : document.querySelector("#playlistSelect"),
            playlistTab : document.querySelector("#playlistTab"),
            hotkeyTab : document.querySelector("#hotkeyTab"),
            playPauseHotkeyButton : document.querySelector("#playPauseHotkey"),
            seekForwardHotkeyButton : document.querySelector("#seekForwardHotkey"),
            seekBackwardHotkeyButton : document.querySelector("#seekBackwardHotkey"),
            jumpForwardHotkeyButton : document.querySelector("#jumpForwardHotkey"),
            jumpBackwardHotkeyButton : document.querySelector("#jumpBackwardHotkey"),
            stopHotkeyButton : document.querySelector("#stopHotkey"),
            volumeUpHotkeyButton : document.querySelector("#volumeUpHotkey"),
            volumeDownHotkeyButton : document.querySelector("#volumeDownHotkey") ,
            muteHotkeyButton: document.querySelector("#muteHotkey"),
            repeatHotkeyButton : document.querySelector("#repeatHotkey"),
            prevTrackHotkeyButton : document.querySelector("#previousTrackHotkey"),
            nextTrackHotkeyButton : document.querySelector("#nextTrackHotkey"),
            fullscreenHotkeyButton : document.querySelector("#fullscreenHotkey"),
            popOutHotkeyButton : document.querySelector("#popOutHotkey"),
            lightsSwitchHotkeyButton : document.querySelector("#theaterModeHotkey"),
            addToPlaylistHotkeyButton : document.querySelector("#addToPlaylistHotkey"),
            optionsHotkeyButton : document.querySelector("#settingsHotkey")
        });

        view = new App.View({
            video : document.querySelector(".video"),
            controlbars : document.querySelectorAll(".controlbar"),
            playerContainer : document.querySelector(".playerContainer"),
            optionsContainer : document.querySelector(".optionsContainer"),
            videoContainer : document.querySelector(".videoContainer"),
            promptContainer : document.querySelector(".promptContainer"),
            templateButton : document.querySelector("#placeholder-item").innerHTML,
            templateSeekBar : document.querySelector("#placeholder-seek-bar").innerHTML,
            templateVolumeBar : document.querySelector("#placeholder-volume-bar").innerHTML,
            templateDoubleButton : document.querySelector("#placeholder-item-double").innerHTML,
            playPauseSelect : document.querySelector("#featurePlay"),
            volumeSelect : document.querySelector("#featureVolume"),
            stopSelect : document.querySelector("#featureStop"),
            seekbarSelect : document.querySelector("#featureSeekbar"),
            repeatSelect : document.querySelector("#featureRepeat"),
            prevNextSelect : document.querySelector("#featurePrevNext"),
            fullscreenSelect : document.querySelector("#featureFullscreen"),
            popOutSelect : document.querySelector("#featurePopOut"),
            lightSwitchSelect : document.querySelector("#featureLightSwitch"),
            addToPlaylistSelect : document.querySelector("#featureAddToPlaylist"),
            optionsFeatureSelect : document.querySelector("#featureOptionsFeature"),
            backgroundImageSelect : document.querySelector(".imageUpload"),
            controllBarPositionSelect : document.querySelector("#featureControllBarPosition"),
            highlightColorJSElement : document.querySelector(".highlightColor"),
            backgroundColorJSElement : document.querySelector(".backgroundColor"),
            buttonColorJSElement : document.querySelector(".buttonColor"),
            buttonStyleSelect : document.querySelector("#buttonStyle"),
            themeSelect : document.querySelector("#themeSelect"),
            featureImages : document.querySelectorAll(".featureImage"),
            playlistContainer : document.querySelector(".playlistContainer"),
            playlistControlbar : document.querySelector(".playlistControlbar"),
            playlistEntries : document.querySelector(".playlistEntries"),
            templatePlaylistEntry : document.querySelector("#playlist-entry").innerHTML,
            playlistSelect : document.querySelector("#playlistSelect"),
            playlistTab : document.querySelector("#playlistTab"),
            cusp : document.querySelector(".cusp"),
            hotkeyTab : document.querySelector("#hotkeyTab"),
            playPauseHotkeyButton : document.querySelector("#playPauseHotkey"),
            seekForwardHotkeyButton : document.querySelector("#seekForwardHotkey"),
            seekBackwardHotkeyButton : document.querySelector("#seekBackwardHotkey"),
            jumpForwardHotkeyButton : document.querySelector("#jumpForwardHotkey"),
            jumpBackwardHotkeyButton : document.querySelector("#jumpBackwardHotkey"),
            stopHotkeyButton : document.querySelector("#stopHotkey"),
            volumeUpHotkeyButton : document.querySelector("#volumeUpHotkey"),
            volumeDownHotkeyButton : document.querySelector("#volumeDownHotkey") ,
            muteHotkeyButton: document.querySelector("#muteHotkey"),
            repeatHotkeyButton : document.querySelector("#repeatHotkey"),
            prevTrackHotkeyButton : document.querySelector("#previousTrackHotkey"),
            nextTrackHotkeyButton : document.querySelector("#nextTrackHotkey"),
            fullscreenHotkeyButton : document.querySelector("#fullscreenHotkey"),
            popOutHotkeyButton : document.querySelector("#popOutHotkey"),
            lightsSwitchHotkeyButton : document.querySelector("#theaterModeHotkey"),
            addToPlaylistHotkeyButton : document.querySelector("#addToPlaylistHotkey"),
            optionsHotkeyButton : document.querySelector("#settingsHotkey")
        });
        model = new App.Model({
            settings : initialSettings,
            video : document.querySelector(".video")
        });
        controlbarController = new App.ControlbarController({
            controlbars : document.querySelectorAll(".controlbar")
        });
        video = new App.Video({
            video: document.querySelector(".video"),
            videoContainer: document.querySelector(".videoContainer"),
        });
        hotkeys = new App.Hotkeys();
        server = new App.Server();
    }
    
     //Tab Events
    function onFeatureTabPressed() {
        view.setTab("Feature");
    }
    
    function onStyleTabPressed() {
        view.setTab("Style");
    }
    
    function onHotkeyTabPressed() {
        view.setTab("Hotkey");
    }
    
    function onPlaylistTabPressed() {
        view.setTab("Playlist");
    }
    
    //color change events
    function onSelectedBackgroundColor(backgroundColor) {
        model.changeBackgroundColor(backgroundColor.data);
    }
    
    function onSelectedHighlightColor(highlightColor) {
        model.changeHighlightColor(highlightColor.data);
    }
    
    function onSelectedButtonColor(buttonColor){
        model.changeButtonColor(buttonColor.data);
    }
    
    function onChangeHighlightColor(highlightColor) {
        view.changeHighlightColor(highlightColor.data);
        view.changeStyleHighlightColor(highlightColor.data);
    }
    
    function onChangeBackgroundColor(backgroundColor) {
        view.changeBackgroundColor(backgroundColor.data);
        view.changeStyleBackgroundColor(backgroundColor.data);
    }
    
    function onChangeButtonColor(buttonColor){
        view.changeButtonColor(buttonColor.data);
    }

    //Controlbar Action Events
    function onFullscreenPressed() {
        model.onFullscreenPressed();
    }
    
    function onToggleFullscreen(){
        view.toggleFullscreen();
    }
    
    function onToggleTheatermode() {
        view.toggleTheatermode();
    }
    
    function onTheatermodePressed() {
        model.onTheatermodePressed();
    }
    
    function onEscPressed() {
        view.onFullscreenExit();
    }
    
    function onOptionsPressed() {
        view.hideOptions();
    }
    
    function onSetVolume(volume) {
        video.setVolume(volume.data);
        view.setVolumeBar(volume.data);
    }

    function onUpdateMuted(muted) {
        video.updateMuted(muted.data);
    }
    
    function onPlayPressed() {
        model.changePlayStateVideo();
    }

    function onPauseVideo() {
        view.pauseVideo();
    }
    
    function onPlayVideo() {
        view.playVideo();
    }
    
    function onStopPressed(){
        model.stopPressed();
    }
    
    function onRepeatPressed(){
        model.repeatPressed();
    }
    
    function onRepeatVideo(isOnRepeat){
        view.repeatVideo(isOnRepeat.data);
    }
    
    function onHideControlbar(){
        view.hideControlbar();
    }
    
    function onShowControlbar(){
        view.showControlbar();
    }
    
    function onStopVideo(){
        view.stopVideo();
    }
    
    function onSeekbarUp(){
        view.onSeekRelease();
    }
    
    function onSeekbarRelease(){
        model.onSeekbarUp();
    }
    
    function onSeekbarChanged(seekbarValue){
        model.changeSeekbarValue(seekbarValue.data);
    }
    
    function onSeekForwardPressed() {
        model.seekForwardPressed();
    }

    function onSeekBackwardPressed() {
        model.seekBackwardPressed();
    }

    function onJumpForwardPressed() {
        model.jumpForwardPressed();
    }

    function onJumpBackwardPressed() {
        model.jumpBackwardPressed();
    }

    function onSetTime(time){
        video.setTime(time.data);
        //view.setVideoTime(time.data);
    }

    function onUpdatePlaybackRate(rate) {
        video.updatePlaybackRate(rate.data);
    }
    
    function onVolumePressed(volumeValue){
        model.setVolume(volumeValue.data);
    }
    
    function onVolumeUp() {
        model.volumeUp();
    }

    function onVolumeDown() {
        model.volumeDown();
    }

    function onMutePressed() {
        model.mutePressed();
    }

    //Controlbar Features Position Events
    function onChangePlayPauseButtonPosition(playPauseButtonPosition) {
        view.changePlayPauseButtonPosition(playPauseButtonPosition.data);
        view.changeFeaturePlayPauseMenu(playPauseButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeSeekBarPosition(seekBarPosition) {
        view.changeSeekBarPosition(seekBarPosition.data);
        view.changeFeatureSeekBarMenu(seekBarPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeStopButtonPosition(stopButtonPosition) {
        view.changeStopButtonPosition(stopButtonPosition.data);
        view.changeFeatureStopMenu(stopButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeVolumePosition(volumePosition) {
        view.changeVolumePosition(volumePosition.data);
        view.changeFeatureVolumeMenu(volumePosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeRepeatButtonPosition(repeatButtonPosition) {
        view.changeRepeatButtonPosition(repeatButtonPosition.data);
        view.changeFeatureRepeatMenu(repeatButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangePrevNextButtonsPosition(prevNextButtonsPosition) {
        view.changePrevNextButtonsPosition(prevNextButtonsPosition.data);
        view.changeFeaturePrevNextMenu(prevNextButtonsPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeFullscreenButtonPosition(fullscreenButtonPosition) {
        view.changeFullscreenButtonPosition(fullscreenButtonPosition.data);
        view.changeFeatureFullscreenMenu(fullscreenButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangePopOutButtonPosition(popOutButtonPosition) {
        view.changePopOutButtonPosition(popOutButtonPosition.data);
        view.changeFeaturePopOutMenu(popOutButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeTheaterModePosition(theaterModePosition) {
        view.changeTheaterModePosition(theaterModePosition.data);
        view.changeFeatureTheaterModeMenu(theaterModePosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeAddToPlaylistPosition(addToPlaylistPosition) {
        view.changeAddToPlaylistPosition(addToPlaylistPosition.data);
        view.changeFeatureAddToPlaylistMenu(addToPlaylistPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeOptionsButtonPosition(optionsButtonPosition) {
        view.changeOptionsButtonPosition(optionsButtonPosition.data);
        view.changeFeatureOptionsFeatureMenu(optionsButtonPosition.data);
        view.disableSelectedPositions();
    }
    
    function onChangeControllBarPosition(controllBarPosition){
        view.changeControllBarPosition(controllBarPosition.data);
        view.changeFeatureContralBarPositionMenu(controllBarPosition.data);
    }
    
    function onChangePlaylistPosition(playlistPosition){
        view.changePlaylistPosition(playlistPosition.data);
        view.changePlaylistPositionMenu(playlistPosition.data);
    }

    function onChangePlaylistControlbar(isActivated) {
        view.changePlaylistControlbar(isActivated.data);
    }

    function onChangePlaylistAddToPlaylistButton(isActivated) {
        view.changePlaylistAddToPlaylistButton(isActivated.data);
    }

    function onChangePlaylistAutoPlayButton(isActivated) {
        view.changePlaylistAutoPlayButton(isActivated.data);
    }

    function onChangePlaylistPrevNextButtons(isActivated) {
        view.changePlaylistPrevNextButtons(isActivated.data);
    }

    function onChangePlaylistRepeatButtons(isActivated) {
        view.changePlaylistRepeatButtons(isActivated.data);
    }

    function onChangePlaylistShuffleButton(isActivated) {
        view.changePlaylistShuffleButton(isActivated.data);
    }

    function onChangePlaylistReverseOrderButton(isActivated) {
        view.changePlaylistReverseOrderButton(isActivated.data);
    }

    // Video Events
    function onVideoLoadedMetaData() {
        view.enableDisableSeekBar();
    }

    function onVideoEmptied() {
        view.enableDisableSeekBar();
    }

    function onVideoTimeUpdate(time) {
        model.updateTime(time.data);
    }

    function onUpdateTimeBar(time) {
        view.updateTimeBar(time.data);
    }

    function onVideoEnded() {
        model.handleEndOfVideo();
    }
    
    function onVolumeChanged(){
        view.onVolumeChange();
    }
    
    function onResetIdleTime(event){
        model.onResetIdleTime(event.data);
    }

    // Playlist Events
    function onRepeatPlaylistPressed() {
        model.toggleRepeatPlaylist();
    }

    function onShufflePressed() {
        model.shufflePlaylist();
    }

    function onPrevPressed() {
        model.playPrev();
    }

    function onNextPressed() {
        model.playNext();
    }
    
    function onChangeIsFullscreen(isFullscreen){
        model.changeFullscreenState(isFullscreen.data);
    }

    function onReversePressed() {
        model.reversePlaylist();
    }

    function onAddToPlaylistPressed() {
        model.addToPlaylistPressed();
    }

    function onAutoplayPlaylistPressed() {
        model.toggleAutoplayPlaylist();
    }

    function onPlaylistEntryClicked(playlistEntryPosition) {
        model.playPlaylistEntry(playlistEntryPosition.data);
    }
    
    function onPlaylistEntryDeleteClicked(playlistEntryPosition) {
        model.deletePlaylistEntry(playlistEntryPosition.data);
    }

    function onUpdatePlaylist(playlist) {
        view.updatePlaylist(playlist.data);
    }
    
    function onChangeVideo(src) {
        view.changeVideo(src.data);
    }

    function onEndVideo() {
        view.endVideo();
    }

    function onPromptItem(promptIdentifier) {
        view.promptItem(promptIdentifier.data);
    }

    function onThemeTaken(isTaken) {
        view.themeTaken(isTaken.data);
    }

    function onSelectPlaylistEntry(position) {
        view.selectPlaylistEntry(position.data);
    }

    function onChangeRepeatPlaylist(isOnRepeat) {
        view.changeRepeatPlaylist(isOnRepeat.data);
    }

    function onChangeAutoplayPlaylist(isOnAutoplay) {
        view.changeAutoplayPlaylist(isOnAutoplay.data);
    }

    function onURLEntered(input) {
        server.getURLData(input.data);
    }

    function onClosePrompt() {
        model.closePrompt();
    }

    function onURLDataReceived(linkData) {
        model.addToPlaylist(linkData.data);
    }

    function onSelectedTheme(themeName) {
        server.getTheme(themeName.data);
    }

    function onThemeReceived(theme) {
        model.changeSettings(theme.data);
    }

    function onSaveThemePressed() {
        model.saveThemePressed();
    }

    function onConfirmTheme(confirmTheme) {
        server.confirmTheme(confirmTheme.data);
    }

    function onConfirmedTheme(confirmTheme) {
        model.confirmTheme(confirmTheme.data);
    }

    function onSavedTheme(settings) {
        model.savedTheme(settings.data);
    }
    
    function onSaveTheme(settings) {
        server.saveTheme(settings.data);
    }

    function onChangedThemeName(themeName) {
        view.changeThemeName(themeName.data);
    }

    function onChangeButtonStyle(buttonStyle) {
        view.changeControllBarButtonStyle(buttonStyle.data);
        view.changeStyleButtonStyle(buttonStyle.data);
    }
    
    //Hotkey onchanges
    function onUpdateHotkeys(hotkeySettings) {
        hotkeys.updateHotkeys(hotkeySettings.data);
        view.updateHotkeys(hotkeySettings.data);
    }
    
    function onSelectedPlayPauseButtonPosition(playPauseButtonPosition){
        model.changePlayPauseButtonPosition(playPauseButtonPosition.data);
    }
    
    function onSelectedSeekBarPosition(seekBarPosition){
        model.changeSeekBarPosition(seekBarPosition.data);
    }
    
    function onSelectedStopButtonPosition(stopButtonPosition){
        model.changeStopButtonPosition(stopButtonPosition.data);
    }
    
    function onSelectedVolumePosition(volumePosition){
        model.changeVolumePosition(volumePosition.data);
    }
    
    function onSelectedRepeatButtonPosition(repeatButtonPosition){
        model.changeRepeatButtonPosition(repeatButtonPosition.data);
    }
    
    function onSelectedPrevNextButtonsPosition(prevNextButtonsPosition){
        model.changePrevNextButtonsPosition(prevNextButtonsPosition.data);
    }
    
    function onSelectedFullscreenButtonPosition(fullscreenButtonPosition){
        model.changeFullscreenButtonPosition(fullscreenButtonPosition.data);
    }
    
    function onSelectedPopOutButtonPosition(popOutButtonPosition){
        model.changePopOutButtonPosition(popOutButtonPosition.data);
    }
    
    function onSelectedAddToPlaylistPosition(addToPlaylistPosition){
        model.changeAddToPlaylistPosition(addToPlaylistPosition.data);
    }
    
    function onSelectedTheaterModePosition(theaterModePosition){
        model.changeTheaterModePosition(theaterModePosition.data);
    }
    
    function onSelectedOptionsButtonPosition(optionsButtonPosition){
        model.changeOptionsButtonPosition(optionsButtonPosition.data);
    }
    
    function onSelectedControllBarPosition(controllBarPosition){
        model.changeControllBarPosition(controllBarPosition.data);
    }
    
    function onSelectedButtonStyle(buttonStyle){
        model.changeButtonStyle(buttonStyle.data);
    }
    
    function onSelectedBackgroundImage(backgroundImage){
        view.changeBackgroundImage(backgroundImage.data);
    }
    
    function onSelectedPlaylistPosition(playlistPosition){
        model.changePlaylistPosition(playlistPosition.data);
    }
    
    function onChangedPlaylistControlbarOption(isChecked) {
        model.changePlaylistControlbar(isChecked.data);
    }

    function onChangedAddToPlaylistOption(isChecked) {
        model.changePlaylistAddToPlaylistButton(isChecked.data);
    }

    function onChangedAutoplayPlaylistOption(isChecked) {
        model.changePlaylistAutoplayButton(isChecked.data);
    }

    function onChangedPrevNextOption(isChecked) {
        model.changePlaylistPrevNextButtons(isChecked.data);
    }

    function onChangedRepeatOption(isChecked) {
        model.changePlaylistRepeatButton(isChecked.data);
    }

    function onChangedShuffleOption(isChecked) {
        model.changePlaylistShuffleButton(isChecked.data);
    }

    function onChangedReverseOption(isChecked) {
        model.changePlaylistReverseOrderButton(isChecked.data);
    }

    //onEnteredMethods Hotkeys
    function onClickedHotkeyButton(target) {
        model.clickedHotkeyButton(target.data);
    }

    function onRecordHotkey() {
        hotkeys.recordHotkey();
    }

    function onHotkeyTaken() {
        view.hotkeyTaken();
    }
    
    function onConfirmHotkey(hotkey) {
        model.changeRecordingHotkey(hotkey.data);
    }
    
    function addTabListeners() {
        controller.addEventListener("featureTabPressed", onFeatureTabPressed);
        controller.addEventListener("styleTabPressed", onStyleTabPressed);
        controller.addEventListener("hotkeyTabPressed", onHotkeyTabPressed);
        controller.addEventListener("playlistTabPressed", onPlaylistTabPressed);
        controller.addEventListener("escPressed", onEscPressed);
    }
    
    function addControlbarListeners() {
        controlbarController.addEventListener("playPressed", onPlayPressed);
        controlbarController.addEventListener("fullscreenPressed", onFullscreenPressed);
        controlbarController.addEventListener("optionsPressed", onOptionsPressed);
        controlbarController.addEventListener("seekbarChanged", onSeekbarChanged);
        controlbarController.addEventListener("seekbarRelease", onSeekbarRelease);
        controlbarController.addEventListener("volumeChanged", onVolumePressed);
        controlbarController.addEventListener("stopPressed", onStopPressed);
        controlbarController.addEventListener("repeatPressed", onRepeatPressed);
        controlbarController.addEventListener("theatermodePressed", onTheatermodePressed);
        controlbarController.addEventListener("prevPressed", onPrevPressed);
        controlbarController.addEventListener("nextPressed", onNextPressed);
        controlbarController.addEventListener("addToPlaylistPressed", onAddToPlaylistPressed);
    }
    
    function addViewListeners() {
        view.addEventListener("changeIsFullscreen", onChangeIsFullscreen);
    }
    
    function addModelListeners() {
        model.addEventListener("updatePlaylist", onUpdatePlaylist);
        model.addEventListener("changeVideo", onChangeVideo);
        model.addEventListener("selectPlaylistEntry", onSelectPlaylistEntry);
        model.addEventListener("changeRepeatPlaylist", onChangeRepeatPlaylist);
        model.addEventListener("changeAutoplayPlaylist", onChangeAutoplayPlaylist);
        model.addEventListener("endVideo", onEndVideo);
        model.addEventListener("promptItem", onPromptItem);
        model.addEventListener("themeTaken", onThemeTaken);
        model.addEventListener("saveTheme", onSaveTheme);
        model.addEventListener("toggleFullscreen", onToggleFullscreen);

        model.addEventListener("pauseVideo", onPauseVideo);
        model.addEventListener("playVideo", onPlayVideo);
        model.addEventListener("updateTimeBar", onUpdateTimeBar);
        model.addEventListener("setTime", onSetTime);
        model.addEventListener("seekbarUp", onSeekbarUp);
        model.addEventListener("updatePlaybackRate", onUpdatePlaybackRate);
        model.addEventListener("setVolume", onSetVolume);
        model.addEventListener("updateMuted", onUpdateMuted);
        model.addEventListener("stopVideo", onStopVideo);
        model.addEventListener("repeatVideo", onRepeatVideo);
        model.addEventListener("toggleTheatermode", onToggleTheatermode);
        model.addEventListener("hideControlbar", onHideControlbar);
        model.addEventListener("showControlbar", onShowControlbar)
        
        model.addEventListener("changePlayPauseButtonPosition", onChangePlayPauseButtonPosition);
        model.addEventListener("changeSeekBarPosition", onChangeSeekBarPosition);
        model.addEventListener("changeStopButtonPosition", onChangeStopButtonPosition);
        model.addEventListener("changeVolumePosition", onChangeVolumePosition);
        model.addEventListener("changeRepeatButtonPosition", onChangeRepeatButtonPosition);
        model.addEventListener("changePrevNextButtonsPosition", onChangePrevNextButtonsPosition);
        model.addEventListener("changeFullscreenButtonPosition", onChangeFullscreenButtonPosition);
        model.addEventListener("changePopOutButtonPosition", onChangePopOutButtonPosition);
        model.addEventListener("changeTheaterModePosition", onChangeTheaterModePosition);
        model.addEventListener("changeAddToPlaylistPosition", onChangeAddToPlaylistPosition);
        model.addEventListener("changeOptionsButtonPosition", onChangeOptionsButtonPosition);
        model.addEventListener("changeControllBarPosition" , onChangeControllBarPosition);

        model.addEventListener("changeHighlightColor", onChangeHighlightColor);
        model.addEventListener("changeBackgroundColor", onChangeBackgroundColor);
        model.addEventListener("changeButtonStyle", onChangeButtonStyle);
        model.addEventListener("changedThemeName", onChangedThemeName);
        model.addEventListener("changeButtonColor", onChangeButtonColor);

        model.addEventListener("recordHotkey", onRecordHotkey);
        model.addEventListener("hotkeyTaken", onHotkeyTaken);
        model.addEventListener("updateHotkeys", onUpdateHotkeys);
        
        model.addEventListener("changePlaylistPosition", onChangePlaylistPosition);
        model.addEventListener("changePlaylistControlbar", onChangePlaylistControlbar);
        model.addEventListener("changePlaylistAddToPlaylistButton", onChangePlaylistAddToPlaylistButton);
        model.addEventListener("changePlaylistAutoPlayButton", onChangePlaylistAutoPlayButton);
        model.addEventListener("changePlaylistPrevNextButtons", onChangePlaylistPrevNextButtons);
        model.addEventListener("changePlaylistRepeatButtons", onChangePlaylistRepeatButtons);
        model.addEventListener("changePlaylistShuffleButton", onChangePlaylistShuffleButton);
        model.addEventListener("changePlaylistReverseOrderButton", onChangePlaylistReverseOrderButton);
    }
        
    function addOptionListeners() {
        controller.addEventListener("selectedPlayPausePosition", onSelectedPlayPauseButtonPosition);
        controller.addEventListener("selectedSeekBarPosition", onSelectedSeekBarPosition);
        controller.addEventListener("selectedStopPosition", onSelectedStopButtonPosition);
        controller.addEventListener("selectedVolumePosition", onSelectedVolumePosition);
        controller.addEventListener("selectedRepeatPosition", onSelectedRepeatButtonPosition);
        controller.addEventListener("selectedPrevNextPosition", onSelectedPrevNextButtonsPosition);
        controller.addEventListener("selectedFullscreenPosition", onSelectedFullscreenButtonPosition);
        controller.addEventListener("selectedPopOutPosition", onSelectedPopOutButtonPosition);
        controller.addEventListener("selectedLightSwitchPosition", onSelectedTheaterModePosition);
        controller.addEventListener("selectedAddToPlaylistPosition", onSelectedAddToPlaylistPosition);
        controller.addEventListener("selectedOptionsPosition", onSelectedOptionsButtonPosition);
        controller.addEventListener("selectedControllbarPosition", onSelectedControllBarPosition);
        
        controller.addEventListener("selectedHighlightColor", onSelectedHighlightColor);
        controller.addEventListener("selectedBackgroundColor", onSelectedBackgroundColor);
        controller.addEventListener("selectedButtonColor",onSelectedButtonColor);
        controller.addEventListener("selectedButtonStyle", onSelectedButtonStyle);
        controller.addEventListener("selectedTheme", onSelectedTheme);
        controller.addEventListener("saveThemePressed", onSaveThemePressed);
        controller.addEventListener("confirmTheme", onConfirmTheme);
        controller.addEventListener("backgroundImageSelected", onSelectedBackgroundImage);

        controller.addEventListener("clickedHotkeyButton", onClickedHotkeyButton);

        controller.addEventListener("selectedPlaylistPosition", onSelectedPlaylistPosition);
        controller.addEventListener("changedPlaylistControlbarOption", onChangedPlaylistControlbarOption);
        controller.addEventListener("changedAddToPlaylistOption", onChangedAddToPlaylistOption);
        controller.addEventListener("changedAutoplayPlaylistOption", onChangedAutoplayPlaylistOption);
        controller.addEventListener("changedPrevNextOption", onChangedPrevNextOption);
        controller.addEventListener("changedRepeatOption", onChangedRepeatOption);
        controller.addEventListener("changedShuffleOption", onChangedShuffleOption);
        controller.addEventListener("changedReverseOption", onChangedReverseOption);
    }

    function addPlaylistListeners() {
        controller.addEventListener("addToPlaylistPressed", onAddToPlaylistPressed);
        controller.addEventListener("autoplayPlaylistPressed", onAutoplayPlaylistPressed);
        controller.addEventListener("prevPressed", onPrevPressed);
        controller.addEventListener("nextPressed", onNextPressed);
        controller.addEventListener("repeatPlaylistPressed", onRepeatPlaylistPressed);
        controller.addEventListener("shufflePressed", onShufflePressed);
        controller.addEventListener("reversePressed", onReversePressed);
        controller.addEventListener("urlEntered", onURLEntered);
        controller.addEventListener("closePrompt", onClosePrompt);
        controller.addEventListener("clickedPlaylistEntry", onPlaylistEntryClicked);
        controller.addEventListener("clickedPlaylistEntryDelete", onPlaylistEntryDeleteClicked);
    }

    function addVideoListeners() {
        video.addEventListener("videoLoadedMetaData", onVideoLoadedMetaData);
        video.addEventListener("videoEmtpied", onVideoEmptied);
        video.addEventListener("videoTimeUpdate", onVideoTimeUpdate);
        video.addEventListener("videoEnded", onVideoEnded);
        video.addEventListener("volumeChanged", onVolumeChanged);
        video.addEventListener("resetIdleTime", onResetIdleTime);
    }

    function addHotkeysListener() {
        hotkeys.addEventListener("closePrompt", onClosePrompt);
        hotkeys.addEventListener("confirmHotkey", onConfirmHotkey);
        hotkeys.addEventListener("playPressed", onPlayPressed);
        hotkeys.addEventListener("seekForwardPressed", onSeekForwardPressed);
        hotkeys.addEventListener("seekBackwardPressed", onSeekBackwardPressed);
        hotkeys.addEventListener("jumpForwardPressed", onJumpForwardPressed);
        hotkeys.addEventListener("jumpBackwardPressed", onJumpBackwardPressed);
        hotkeys.addEventListener("stopPressed", onStopPressed);
        hotkeys.addEventListener("volumeUp", onVolumeUp);
        hotkeys.addEventListener("volumeDown", onVolumeDown);
        hotkeys.addEventListener("mutePressed", onMutePressed);
        hotkeys.addEventListener("repeatPressed", onRepeatPressed);
        hotkeys.addEventListener("prevPressed", onPrevPressed);
        hotkeys.addEventListener("nextPressed", onNextPressed);
        hotkeys.addEventListener("fullscreenPressed", onFullscreenPressed);
        hotkeys.addEventListener("addToPlaylistPressed", onAddToPlaylistPressed);
        hotkeys.addEventListener("theatermodePressed", onTheatermodePressed);
        hotkeys.addEventListener("optionsPressed", onOptionsPressed);
    }

    function addServerListerners() {
        server.addEventListener("urlDataReceived", onURLDataReceived);
        server.addEventListener("themeReceived", onThemeReceived);
        server.addEventListener("confirmedTheme", onConfirmedTheme);
        server.addEventListener("savedTheme", onSavedTheme);
    }

    function addAllListeners() {
        addTabListeners();
        addControlbarListeners();
        addModelListeners();
        addOptionListeners();
        addViewListeners();
        addPlaylistListeners();
        addVideoListeners();
        addHotkeysListener();
        addServerListerners();
    }

    function fillPlaylist() {
        var i,
            links = [
                "https://www.youtube.com/watch?v=5d3nASKtGas",
                "http://www.dailymotion.com/video/x4mx1xz_mind-blowing-rooftop-parkour-w-oleg-cricket-urbex_sport",
                "https://vimeo.com/174304761",
                "https://www.youtube.com/watch?v=8lWpnvNxs8k",
                "https://www.tagesschau.de/100sekunden/index.html",
                "https://www.youtube.com/watch?v=5d3nASKtGas",
                "https://www.youtube.com/watch?v=5d3nASKtGas",
            ];

        for (i = 0; i < links.length; i++) {
            onURLEntered({data: links[i]});
        }
        onPlayVideo();
    }
    
    function init(settings) {
        var mySVGsToInject,
            myPlaylistSVGsToInject;
        initialSettings = settings;

        setUp();

        controller.init();
        controlbarController.init();
        video.init();
        addAllListeners();
        
        model.init();
        mySVGsToInject = document.querySelectorAll("img.controllBarImage");
        myPlaylistSVGsToInject = document.querySelectorAll(".playlistControlbar img");
        SVGInjector(mySVGsToInject);
        SVGInjector(myPlaylistSVGsToInject);
        fillPlaylist();
    }

    that.init = init;
    return that;
}());
