var App = App || {};
App.Hotkeys = function () {
    "use strict";
    /* eslint-env browser */
    /* global _, EventPublisher, Mousetrap */

    var that = new EventPublisher;

    function recordHotkey() {
        var hotkey;
        Mousetrap.record(function (sequence) {
            hotkey = sequence[0];
            switch (hotkey) {
            case "esc":
                that.notifyAll("closePrompt");
                return;
            case "backspace":
                hotkey = "Kein Hotkey";
                break;
            default:
                break;
            }
            that.notifyAll("confirmHotkey", hotkey);
        });
    }

    function updateHotkeys(hotkeys) {
        Mousetrap.reset();
        _.each(hotkeys, function(hotkey, hotkeyName) {
            switch (hotkeyName) {
            case "playPause":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("playPressed");
                });
                break;
            case "seekForward":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("seekForwardPressed");
                });
                break;
            case "seekBackward":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("seekBackwardPressed");
                });
                break;
            case "jumpForward":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("jumpForwardPressed");
                });
                break;
            case "jumpBackward":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("jumpBackwardPressed");
                });
                break;
            case "stop":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("stopPressed");
                });
                break;
            case "volumeUp":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("volumeUp");
                });
                break;
            case "volumeDown":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("volumeDown");
                });
                break;
            case "mute":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("mutePressed");
                });
                break;
            case "repeat":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("repeatPressed");
                });
                break;
            case "prevTrack":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("prevPressed");
                });
                break;
            case "nextTrack":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("nextPressed");
                });
                break;
            case "fullscreen":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("fullscreenPressed");
                });
                break;
            case "popOut":
                Mousetrap.bind(hotkey, function() {
                    console.log("popOut - Hotkey.js");
                    //that.notifyAll("");
                });
                break;
            case "addToPlaylist":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("addToPlaylistPressed");
                }, "keyup");
                break;
            case "theaterMode":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("theatermodePressed");
                });
                break;
            case "options":
                Mousetrap.bind(hotkey, function() {
                    that.notifyAll("optionsPressed");
                });
                break;
            default:
                break;
            }
        });
    }

    that.recordHotkey = recordHotkey;
    that.updateHotkeys = updateHotkeys;
    return that;
};
