var App = App || {};
App.Model = function (options) {
    "use strict";
    /* eslint-env browser */
    /* global _, EventPublisher */
    
    var that = new EventPublisher(),
        settings = options.settings,
        playlist = [],
        selectedPosition,
        selectedSrc,
        timeoutID,
        blockHide = false,
        repeatVideo = false,
        repeatPlaylist = false,
        isFullscreen = false,
        autoplayPlaylist = true,
        currentPrompt = "",
        isRecordingHotkey = "",
        playbackRate = 1,
        volume = 1,
        muted = false,
        currentTime = 0,
        video = options.video;

    function init() {
        changeSettings(settings);
        that.notifyAll("changeAutoplayPlaylist", autoplayPlaylist);
        that.notifyAll("changeRepeatPlaylist", repeatPlaylist);
    }

    // playlist events
    function handleEndOfVideo() {
        var isOnLastPosition = playlist.length - 1 === selectedPosition;
        if ((!autoplayPlaylist && !repeatVideo) ||
                (!repeatPlaylist && !repeatVideo && isOnLastPosition)) {
            that.notifyAll("endVideo");
            return;
        }
        playNext();
    }

    function addToPlaylist(linkData) {
        playlist.push(linkData);
        that.notifyAll("updatePlaylist", playlist);
    }

    function playPlaylistEntry(position) {
        playbackRate = 1,
        selectedPosition = position;
        selectedSrc = getVideoSource(selectedPosition);
        that.notifyAll("changeVideo", selectedSrc);
        that.notifyAll("selectPlaylistEntry", selectedPosition);
    }
    
    function deletePlaylistEntry(position) {
        var tmpPos = selectedPosition;
        if (selectedPosition === position) {
            if (selectedPosition === playlist.length - 1
                    && (!repeatPlaylist || playlist.length === 1)) {
                stopPressed();
                selectedPosition = undefined;
            }
            else {
                playNext();
            }
        }
        playlist.splice(position, 1);
        that.notifyAll("updatePlaylist", playlist);
        if (position <= tmpPos && selectedPosition) {
            selectedPosition -= 1;
            that.notifyAll("selectPlaylistEntry", selectedPosition);
        }
    }

    function getVideoSource(position) {
        var videoFormats = playlist[position].data.formats,
            src;

        // useful when we want to implement audioOnly/videoQuality
        _.each(videoFormats, function(format) {
            if(format.vcodec !== "none") {
                src = format.url;
            }
        });
        return src;
    }

    function toggleRepeatPlaylist() {
        repeatPlaylist = !repeatPlaylist;
        that.notifyAll("changeRepeatPlaylist", repeatPlaylist);
    }

    function toggleAutoplayPlaylist() {
        autoplayPlaylist = !autoplayPlaylist;
        that.notifyAll("changeAutoplayPlaylist", autoplayPlaylist);
    }

    function shufflePlaylist() {
        // TODO handle playlist randomly landing in the same place
        // while (!playlistHasChanged) {
        //     shuffleAgain();
        // }
        var i,
            randomPosition,
            selectionHandled = false,
            randomEntry;
        for (i = 0; i < playlist.length; i++) {
            randomPosition = Math.floor(Math.random() * (playlist.length - i));
            if (!selectionHandled) {
                if (randomPosition === selectedPosition) {
                    selectedPosition = i;
                    selectionHandled = true;
                }
                if (randomPosition < selectedPosition && !selectionHandled) {
                    selectedPosition--;
                }
            }
            randomEntry = playlist[randomPosition];
            playlist.splice(randomPosition, 1);
            playlist.push(randomEntry);
        }
        that.notifyAll("updatePlaylist", playlist);
        that.notifyAll("selectPlaylistEntry", selectedPosition);
    }

    function reversePlaylist() {
        playlist.reverse();
        selectedPosition = playlist.length - 1 - selectedPosition;
        that.notifyAll("updatePlaylist", playlist);
        that.notifyAll("selectPlaylistEntry", selectedPosition);
    }

    function playPrev() {
        if (!playlist[selectedPosition - 1]) {
            if (repeatPlaylist) {
                playPlaylistEntry(playlist.length - 1);
            }
        }
        else {
            playPlaylistEntry(selectedPosition - 1);
        }
    }

    function playNext() {
        if (!playlist[selectedPosition + 1]) {
            if (repeatPlaylist) {
                playPlaylistEntry(0);
            }
        }
        else {
            playPlaylistEntry(selectedPosition + 1);
        }
    }
    
    // video action events
    function onTheatermodePressed(){
        that.notifyAll("toggleTheatermode");
    }
    
    function changePlayStateVideo() {
        if (!video.paused){
            that.notifyAll("pauseVideo");
        }   else if (video.paused){
            that.notifyAll("playVideo");
        }
    }
    
    function stopPressed() {
        that.notifyAll("stopVideo");
    }

    function updateTime(time) {
        currentTime = time;
        that.notifyAll("updateTimeBar", currentTime);
    }
    
    function onSeekbarUp(){
        that.notifyAll("seekbarUp");
    }
    
    function changeSeekbarValue(seekbarValue){
        var time = video.duration * (seekbarValue / 100);
        currentTime = time;
        that.notifyAll("setTime",time);
    }

    function seekForwardPressed() {
        if (playbackRate >= 5) {
            return;
        }
        if (playbackRate < 5 && playbackRate >= 1) {
            playbackRate++;
        }
        else {
            playbackRate += 0.15;
        }
        that.notifyAll("updatePlaybackRate", playbackRate);
    }

    function seekBackwardPressed() {
        if (playbackRate <= 0.25) {
            return;
        }
        if (playbackRate > 0.25 && playbackRate <= 1) {
            playbackRate -= 0.15;
        }
        else {
            playbackRate--;
        }
        that.notifyAll("updatePlaybackRate", playbackRate);
    }

    function jumpForwardPressed() {
        currentTime += 10;
        that.notifyAll("setTime", currentTime);
        that.notifyAll("updateTimeBar", currentTime);
    }

    function jumpBackwardPressed() {
        currentTime -= 10;
        that.notifyAll("setTime", currentTime);
        that.notifyAll("updateTimeBar", currentTime);
    }
    
    function setVolume(volumeValue){
        volume = volumeValue;
        if (volume < 0) {
            volume = 0;
        }
        if (volume > 1) {
            volume = 1;
        }
        volume = +(volume.toFixed(2));
        if (muted) {
            mutePressed();
        }
        that.notifyAll("setVolume", volume);
    }

    function volumeUp() {
        volume += 0.1;
        setVolume(volume);
    }

    function volumeDown() {
        volume -= 0.1;
        setVolume(volume);
    }

    function mutePressed() {
        muted = !muted;
        that.notifyAll("updateMuted", muted);
    }
    
    function repeatPressed(){
        repeatVideo = !repeatVideo;
        that.notifyAll("repeatVideo", repeatVideo);
    }

    function addToPlaylistPressed() {
        currentPrompt = ".addLink";
        that.notifyAll("promptItem", currentPrompt);
    }

    function saveThemePressed() {
        currentPrompt = ".saveTheme";
        that.notifyAll("promptItem", currentPrompt);
    }

    function confirmTheme(confirmTheme) {
        var newSettings;
        that.notifyAll("themeTaken", !confirmTheme.isValid);
        if (confirmTheme.isValid && confirmTheme.isSubmitted) {
            newSettings = {
                themeName: confirmTheme.themeName,
                features: settings.features,
                style: settings.style,
                hotkeys: settings.hotkeys,
                playlist: settings.playlist,
                player: settings.player
            };
            newSettings.themeName = confirmTheme.themeName;
            that.notifyAll("saveTheme", newSettings);
            closePrompt();
        }
    }

    function savedTheme(newSettings) {
        changeSettings(newSettings);
        closePrompt();
    }

    function closePrompt() {
        currentPrompt = "";
        if (isRecordingHotkey !== "") {
            isRecordingHotkey = "";
            that.notifyAll("updateHotkeys", settings.hotkeys);
        }
        that.notifyAll("promptItem", currentPrompt);
    }
    
    function onFullscreenPressed(){
        that.notifyAll("toggleFullscreen");
        if (isFullscreen){
            startTimer();
        }
    }
    
    function changeFullscreenState(state){
        isFullscreen = state;
    }
    
    function onResetIdleTime(event){
        if (event.target.classList.contains("placeholder-item")){
            blockHide = true;
        } else blockHide = false;
        window.clearTimeout(timeoutID);
        goActive();
    }
    
    function goActive() {
        if (isFullscreen){
            that.notifyAll("showControlbar"); 
        }
        startTimer();
    }
    
    function goInactive(){
        if (!blockHide){
            that.notifyAll("hideControlbar");
        }
    }
    
    function startTimer() {  
        if (isFullscreen){
            timeoutID = window.setTimeout(goInactive, 2000);
        }
    }
    //funktionen, die neue Werte in Settings einlesen, und über notify eine Veränderung der View auslösen
    //features
    function changePlayPauseButtonPosition(playPauseButtonPosition) {
        settings.features.playPauseButton = playPauseButtonPosition;
        that.notifyAll("changePlayPauseButtonPosition", playPauseButtonPosition);
    }
    
    function changeSeekBarPosition(seekBarPosition) {
        settings.features.seekBar = seekBarPosition;
        that.notifyAll("changeSeekBarPosition", seekBarPosition);
    }
    
    function changeStopButtonPosition(stopButtonPosition) {
        settings.features.stopButton = stopButtonPosition;
        that.notifyAll("changeStopButtonPosition", stopButtonPosition);
    }
    
    function changeVolumePosition(volumePosition) {
        settings.features.volume = volumePosition;
        that.notifyAll("changeVolumePosition", volumePosition);
    }
    
    function changeRepeatButtonPosition(repeatButtonPosition) {
        settings.features.repeatButton = repeatButtonPosition;
        that.notifyAll("changeRepeatButtonPosition", repeatButtonPosition);
    }
    
    function changePrevNextButtonsPosition(prevNextButtonsPosition) {
        settings.features.prevNextButtons = prevNextButtonsPosition;
        that.notifyAll("changePrevNextButtonsPosition", prevNextButtonsPosition);
    }
    
    function changeFullscreenButtonPosition(fullscreenButtonPosition) {
        settings.features.fullscreenButton = fullscreenButtonPosition;
        that.notifyAll("changeFullscreenButtonPosition", fullscreenButtonPosition);
    }
    
    function changePopOutButtonPosition(popOutButtonPosition) {
        settings.features.popOutButton = popOutButtonPosition;
        that.notifyAll("changePopOutButtonPosition", popOutButtonPosition);
    }
    
    function changeTheaterModePosition(theaterModePosition) {
        settings.features.theaterMode = theaterModePosition;
        that.notifyAll("changeTheaterModePosition", theaterModePosition);
    }
    
    function changeAddToPlaylistPosition(addToPlaylistPosition) {
        settings.features.addToPlaylist = addToPlaylistPosition;
        that.notifyAll("changeAddToPlaylistPosition", addToPlaylistPosition);
    }
    
    function changeOptionsButtonPosition(optionsButtonPosition) {
        settings.features.optionsButton = optionsButtonPosition;
        that.notifyAll("changeOptionsButtonPosition", optionsButtonPosition);
    }
    

    function changeHighlightColor(highlightColor) {
        settings.style.highlightColor = highlightColor;
        that.notifyAll("changeHighlightColor", highlightColor);
    }
    
    function changeBackgroundColor(backgroundColor) {
        settings.style.backgroundColor = backgroundColor;
        that.notifyAll("changeBackgroundColor", backgroundColor);
    }
    
    function changeButtonColor(buttonColor) {
        settings.style.buttonColor = buttonColor;
        that.notifyAll("changeButtonColor", buttonColor);
    }
    
    function changeControllBarPosition(controllBarPosition){
        settings.features.controllBarPosition = controllBarPosition;
        that.notifyAll("changeControllBarPosition", controllBarPosition);
    }
    
    function changeButtonStyle(buttonStyle){
        settings.style.buttonStyle = buttonStyle;
        that.notifyAll("changeButtonStyle", buttonStyle);
    }
    
    function clickedHotkeyButton(target) {
        currentPrompt = ".changeHotkey";
        that.notifyAll("promptItem", currentPrompt);
        isRecordingHotkey = target.id;
        that.notifyAll("recordHotkey");
    }

    function changeRecordingHotkey(hotkey) {
        switch (isRecordingHotkey) {
        case "playPauseHotkey":
            if (confirmHotkey(hotkey, "playPause")) {
                settings.hotkeys.playPause = hotkey;
                closePrompt();
                return;
            }
            break;
        case "seekForwardHotkey":
            if (confirmHotkey(hotkey, "seekForward")) {
                settings.hotkeys.seekForward = hotkey;
                closePrompt();
                return;
            }
            break;
        case "seekBackwardHotkey":
            if (confirmHotkey(hotkey, "seekBackward")) {
                settings.hotkeys.seekBackward = hotkey;
                closePrompt();
                return;
            }
            break;
        case "jumpForwardHotkey":
            if (confirmHotkey(hotkey, "jumpForward")) {
                settings.hotkeys.jumpForward = hotkey;
                closePrompt();
                return;
            }
            break;
        case "jumpBackwardHotkey":
            if (confirmHotkey(hotkey, "jumpBackward")) {
                settings.hotkeys.jumpBackward = hotkey;
                closePrompt();
                return;
            }
            break;
        case "stopHotkey":
            if (confirmHotkey(hotkey, "stop")) {
                settings.hotkeys.stop = hotkey;
                closePrompt();
                return;
            }
            break;
        case "volumeUpHotkey":
            if (confirmHotkey(hotkey, "volumeUp")) {
                settings.hotkeys.volumeUp = hotkey;
                closePrompt();
                return;
            }
            break;
        case "volumeDownHotkey":
            if (confirmHotkey(hotkey, "volumeDown")) {
                settings.hotkeys.volumeDown = hotkey;
                closePrompt();
                return;
            }
            break;
        case "muteHotkey":
            if (confirmHotkey(hotkey, "mute")) {
                settings.hotkeys.mute = hotkey;
                closePrompt();
                return;
            }
            break;
        case "repeatHotkey":
            if (confirmHotkey(hotkey, "repeat")) {
                settings.hotkeys.repeat = hotkey;
                closePrompt();
                return;
            }
            break;
        case "nextTrackHotkey":
            if (confirmHotkey(hotkey, "prevTrack")) {
                settings.hotkeys.nextTrack = hotkey;
                closePrompt();
                return;
            }
            break;
        case "previousTrackHotkey":
            if (confirmHotkey(hotkey, "nextTrack")) {
                settings.hotkeys.prevTrack = hotkey;
                closePrompt();
                return;
            }
            break;
        case "fullscreenHotkey":
            if (confirmHotkey(hotkey, "fullscreen")) {
                settings.hotkeys.fullscreen = hotkey;
                closePrompt();
                return;
            }
            break;
        case "popOutHotkey":
            if (confirmHotkey(hotkey, "popOut")) {
                settings.hotkeys.popOut = hotkey;
                closePrompt();
                return;
            }
            break;
        case "addToPlaylistHotkey":
            if (confirmHotkey(hotkey, "theaterMode")) {
                settings.hotkeys.addToPlaylist = hotkey;
                closePrompt();
                return;
            }
            break;
        case "theaterModeHotkey":
            if (confirmHotkey(hotkey, "addToPlaylist")) {
                settings.hotkeys.theaterMode = hotkey;
                closePrompt();
                return;
            }
            break;
        case "settingsHotkey":
            if (confirmHotkey(hotkey, "options")) {
                settings.hotkeys.options = hotkey;
                closePrompt();
                return;
            }
            break;
        default:
            return;
        }
        that.notifyAll("hotkeyTaken");
        that.notifyAll("recordHotkey");
    }
    
    function confirmHotkey(newHotkey, newHotkeyName) {
        var isValid = false;
        if (newHotkey === "Kein Hotkey") {
            return true;
        }
        _.find(settings.hotkeys, function(hotkey, hotkeyName) {
            isValid = (newHotkey === hotkey && newHotkeyName === hotkeyName);
            return newHotkeyName === hotkeyName;
        });
        if (isValid) {
            return isValid;
        }

        _.find(settings.hotkeys, function(hotkey) {
            isValid = !(newHotkey === hotkey);
            return newHotkey === hotkey;
        });
        return isValid;
    }
    
    function changePlaylistPosition(positionPlaylist){
        settings.playlist.position = positionPlaylist;
        that.notifyAll("changePlaylistPosition",positionPlaylist);
    }
    function changePlaylistDisplayDuration(displayDurationPlaylist){
        settings.playlist.displayDuration = displayDurationPlaylist;
    }
    function changePlaylistDisplayLink(displayLinkPlaylist){
        settings.playlist.displayLink = displayLinkPlaylist;
    }
    function changePlaylistControlbar(isChecked) {
        settings.playlist.controlbar = isChecked;
        that.notifyAll("changePlaylistControlbar", settings.playlist.controlbar);
    }
    function changePlaylistAddToPlaylistButton(isChecked) {
        settings.playlist.addToPlaylistButton = isChecked;
        that.notifyAll("changePlaylistAddToPlaylistButton", settings.playlist.addToPlaylistButton);
    }
    function changePlaylistAutoplayButton(isChecked) {
        settings.playlist.autoplayPlaylistButton = isChecked;
        that.notifyAll("changePlaylistAutoPlayButton", settings.playlist.autoplayPlaylistButton);
    }
    function changePlaylistPrevNextButtons(isChecked){
        settings.playlist.prevNextButtons = isChecked;
        that.notifyAll("changePlaylistPrevNextButtons", settings.playlist.prevNextButtons);
    }
    function changePlaylistRepeatButton(isChecked){
        settings.playlist.repeatButton = isChecked;
        that.notifyAll("changePlaylistRepeatButtons", settings.playlist.repeatButton);
    }
    function changePlaylistShuffleButton(isChecked){
        settings.playlist.shuffleButton = isChecked;
        that.notifyAll("changePlaylistShuffleButton", settings.playlist.shuffleButton);
    }
    function changePlaylistReverseOrderButton(isChecked){
        settings.playlist.reverseOrderButton = isChecked;
        that.notifyAll("changePlaylistReverseOrderButton", settings.playlist.reverseOrderButton);
    }
    
    function changeSettings(newSettings) {
        settings.themeName = newSettings.themeName;
        that.notifyAll("changedThemeName", settings.themeName);
        changeFeatureSettings(newSettings);
        changeStyleSettings(newSettings);
        changeHotkeysSettings(newSettings);
        changePlaylistSettings(newSettings);
    }
    
    function changeFeatureSettings(newSettings){
        changePlayPauseButtonPosition(newSettings.features.playPauseButton);
        changeSeekBarPosition(newSettings.features.seekBar);
        changeStopButtonPosition(newSettings.features.stopButton);
        changeVolumePosition(newSettings.features.volume);
        changeRepeatButtonPosition(newSettings.features.repeatButton);
        changePrevNextButtonsPosition(newSettings.features.prevNextButtons);
        changeFullscreenButtonPosition(newSettings.features.fullscreenButton);
        changePopOutButtonPosition(newSettings.features.popOutButton);
        changeTheaterModePosition(newSettings.features.theaterMode);
        changeAddToPlaylistPosition(newSettings.features.addToPlaylist);
        changeOptionsButtonPosition(newSettings.features.optionsButton);
        changeControllBarPosition(newSettings.features.controllBarPosition);
    }
    
    function changeStyleSettings(newSettings){
        changeBackgroundColor(newSettings.style.backgroundColor);
        changeHighlightColor(newSettings.style.highlightColor);
        changeButtonColor(newSettings.style.buttonColor);
        changeButtonStyle(newSettings.style.buttonStyle);
    }
    
    function changeHotkeysSettings(newSettings){
        settings.hotkeys = newSettings.hotkeys;
        that.notifyAll("updateHotkeys", settings.hotkeys);
    }
    
    
    function changePlaylistSettings(newSettings){
        changePlaylistPosition(newSettings.playlist.position);
        changePlaylistDisplayDuration(newSettings.playlist.displayDuration);
        changePlaylistDisplayLink(newSettings.playlist.displayLink);
        changePlaylistControlbar(newSettings.playlist.controlbar);
        changePlaylistAddToPlaylistButton(newSettings.playlist.addToPlaylistButton);
        changePlaylistAutoplayButton(newSettings.playlist.autoplayPlaylistButton);
        changePlaylistPrevNextButtons(newSettings.playlist.prevNextButtons);
        changePlaylistRepeatButton(newSettings.playlist.repeatButton);
        changePlaylistReverseOrderButton(newSettings.playlist.reverseOrderButton);
        changePlaylistShuffleButton(newSettings.playlist.shuffleButton);
    }

    function getSettings() {
        return settings;
    }
    
    
    that.changePlayStateVideo = changePlayStateVideo;
    that.changeSeekbarValue = changeSeekbarValue;
    that.updateTime = updateTime;
    that.onSeekbarUp = onSeekbarUp;
    that.seekForwardPressed = seekForwardPressed;
    that.seekBackwardPressed = seekBackwardPressed;
    that.jumpForwardPressed = jumpForwardPressed;
    that.jumpBackwardPressed = jumpBackwardPressed;
    that.setVolume = setVolume;
    that.volumeUp = volumeUp;
    that.volumeDown = volumeDown;
    that.mutePressed = mutePressed;
    that.stopPressed = stopPressed;
    that.repeatPressed = repeatPressed;
    that.onTheatermodePressed = onTheatermodePressed;
    that.addToPlaylistPressed = addToPlaylistPressed;
    that.saveThemePressed = saveThemePressed;
    that.confirmTheme = confirmTheme;
    that.savedTheme = savedTheme;
    that.closePrompt = closePrompt;
    that.onResetIdleTime = onResetIdleTime;
    that.onFullscreenPressed = onFullscreenPressed;
    that.changeFullscreenState = changeFullscreenState;
    
    that.changePlayPauseButtonPosition = changePlayPauseButtonPosition;
    that.changeSeekBarPosition = changeSeekBarPosition;
    that.changeStopButtonPosition = changeStopButtonPosition;
    that.changeVolumePosition = changeVolumePosition;
    that.changeRepeatButtonPosition = changeRepeatButtonPosition;
    that.changePrevNextButtonsPosition = changePrevNextButtonsPosition;
    that.changeFullscreenButtonPosition = changeFullscreenButtonPosition;
    that.changePopOutButtonPosition = changePopOutButtonPosition;
    that.changeTheaterModePosition = changeTheaterModePosition;
    that.changeAddToPlaylistPosition = changeAddToPlaylistPosition;
    that.changeOptionsButtonPosition = changeOptionsButtonPosition;
    that.changeControllBarPosition = changeControllBarPosition;

    that.changeBackgroundColor = changeBackgroundColor;
    that.changeHighlightColor = changeHighlightColor;
    that.changeButtonStyle = changeButtonStyle;
    that.changeButtonColor = changeButtonColor;

    that.clickedHotkeyButton = clickedHotkeyButton;
    that.changeRecordingHotkey = changeRecordingHotkey;

    that.changePlaylistPosition = changePlaylistPosition;
    that.changePlaylistControlbar = changePlaylistControlbar;
    that.changePlaylistAddToPlaylistButton = changePlaylistAddToPlaylistButton;
    that.changePlaylistAutoplayButton = changePlaylistAutoplayButton;
    that.changePlaylistPrevNextButtons = changePlaylistPrevNextButtons;
    that.changePlaylistRepeatButton = changePlaylistRepeatButton;
    that.changePlaylistReverseOrderButton = changePlaylistReverseOrderButton;
    that.changePlaylistShuffleButton = changePlaylistShuffleButton;
    that.handleEndOfVideo = handleEndOfVideo;
    that.addToPlaylist = addToPlaylist;
    that.playPlaylistEntry = playPlaylistEntry;
    that.toggleAutoplayPlaylist = toggleAutoplayPlaylist;
    that.playPrev = playPrev;
    that.playNext = playNext;
    that.toggleRepeatPlaylist = toggleRepeatPlaylist;
    that.shufflePlaylist = shufflePlaylist;
    that.reversePlaylist = reversePlaylist;
    that.deletePlaylistEntry = deletePlaylistEntry;
    
    that.getSettings = getSettings;
    that.changeSettings = changeSettings;
    that.init = init;
    return that;
};
