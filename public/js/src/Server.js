var App = App || {};
App.Server = function () {
    "use strict";
    /* eslint-env browser */
    /* global _, EventPublisher, qwest  */

    var that = new EventPublisher;

    function getURLData(url) {
        qwest.get("/ytdl?q=" + url, { responseType : "json" })
            .then(function(xhr, response) {
                var linkData = {
                    link : url,
                    data : JSON.parse(response)
                };
                that.notifyAll("urlDataReceived", linkData);
            });
    }

    function getTheme(themeName) {
        qwest.get("/t/?q=" + themeName, { responseType: "json" })
            .then(function(xhr, response) {
                that.notifyAll("themeReceived", response);
            });
    }

    function saveTheme(settings) {
        qwest.post("/save", settings)
            .then(function(xhr, response) {
                that.notifyAll("savedTheme", settings);
            });
    }

    function confirmTheme(confirmThemeData) {
        qwest.get("/t/?q=" + confirmThemeData.themeName)
            .then(function(xhr, response) {
                if (response) {
                    _.extend( confirmThemeData, { isValid: false });
                    that.notifyAll("confirmedTheme", confirmThemeData);
                }
                else {
                    _.extend( confirmThemeData, { isValid: true });
                    that.notifyAll("confirmedTheme", confirmThemeData);
                }
            });
    }

    that.getURLData = getURLData;
    that.getTheme = getTheme;
    that.saveTheme = saveTheme;
    that.confirmTheme = confirmTheme;
    return that;
};
