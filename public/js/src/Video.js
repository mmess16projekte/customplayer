var App = App || {};
App.Video = function (options) {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher, window */
    
    var that = new EventPublisher(),
        video = options.video,
        videoContainer = options.videoContainer;

    function onLoadedMetaData(event) {
        that.notifyAll("videoLoadedMetaData", event);
    }

    function onEmptied(event) {
        that.notifyAll("videoEmtpied", event);
    }

    function onTimeUpdate(event) {
        that.notifyAll("videoTimeUpdate", event.target.currentTime);
    }

    function onEndedVideo(event) {
        that.notifyAll("videoEnded", event);
    }
    
    function onVolumeChange() {
        that.notifyAll("volumeChanged", event);
    }
    
    function resetIdleTimer(){
        that.notifyAll("resetIdleTime", event);
    }

    function init() {
        video.addEventListener("loadedmetadata", onLoadedMetaData, false);
        video.addEventListener("emptied", onEmptied, false);
        video.addEventListener("timeupdate", onTimeUpdate, false);
        video.addEventListener("ended", onEndedVideo, false );
        video.addEventListener("volumechange", onVolumeChange, false);
        
        videoContainer.addEventListener("mousemove", resetIdleTimer, false);
        videoContainer.addEventListener("mousedown", resetIdleTimer, false);
        videoContainer.addEventListener("keypress", resetIdleTimer, false);
        videoContainer.addEventListener("DOMMouseScroll", resetIdleTimer, false);
        videoContainer.addEventListener("mousewheel", resetIdleTimer, false);
        videoContainer.addEventListener("touchmove", resetIdleTimer, false);
        videoContainer.addEventListener("MSPointerMove", resetIdleTimer, false);
    }

    function updatePlaybackRate(playbackRate) {
        video.playbackRate = playbackRate;
    }

    function setVolume(volume) {
        video.volume = volume;
    }

    function updateMuted(muted) {
        video.muted = muted;
    }

    function setTime(time) {
        video.currentTime = time;
    }
    
    that.updatePlaybackRate = updatePlaybackRate;
    that.setVolume = setVolume;
    that.updateMuted = updateMuted;
    that.setTime = setTime;

    that.init = init;
    return that;
};
