var App = App || {};
App.View = function (options) {
    "use strict";
    /* eslint-env browser */
    /* global _, EventPublisher */
    
    var that = new EventPublisher(),
        isFullscreen = false,
        isCover = false,
        duration,
        videoWasPaused,
        blockSeek = false,
        play = false,
        saveVolume,
        video = options.video,
        playlistControlbar = options.playlistControlbar,
        controlbars = options.controlbars,
        playerContainer = options.playerContainer,
        optionsContainer = options.optionsContainer,
        videoContainer = options.videoContainer,
        promptContainer = options.promptContainer,
        templateButton = options.templateButton,
        templateSeekBar = options.templateSeekBar,
        templateVolumeBar = options.templateVolumeBar,
        playPauseSelect = options.playPauseSelect,
        volumeSelect = options.volumeSelect,
        stopSelect = options.stopSelect,
        seekbarSelect = options.seekbarSelect,
        repeatSelect = options.repeatSelect,
        prevNextSelect = options.prevNextSelect,
        fullscreenSelect = options.fullscreenSelect,
        popOutSelect = options.popOutSelect,
        theaterModeSelect = options.lightSwitchSelect,
        addToPlaylistSelect = options.addToPlaylistSelect,
        optionsFeatureSelect = options.optionsFeatureSelect,
        backgroundImageSelect = options.backgroundImageSelect,
        controllBarPositionSelect = options.controllBarPositionSelect,
        backgroundColorJSElement = options.backgroundColorJSElement,
        highlightColorJSElement = options.highlightColorJSElement,
        buttonColorJSElement = options.buttonColorJSElement,
        buttonStyleSelect = options.buttonStyleSelect,
        themeSelect = options.themeSelect,
        playlistContainer = options.playlistContainer,
        playlistEntries = options.playlistEntries,
        templatePlaylistEntry = _.template(options.templatePlaylistEntry),
        playlistSelect = options.playlistSelect,
        playlistTab = options.playlistTab,
        cusp = options.cusp,
        hotkeyTab = options.hotkeyTab;
        
    
    function endVideo() {
        var i,
            seekbars = document.querySelectorAll(".seekbar");
        for (i = 0; i < controlbars.length; i++) {
            controlbars[i].querySelector(".play").className = "play placeholder-item";
            controlbars[i].querySelector(".pause").className = "pause placeholder-item hidden";
            seekbars[i].value = 0;
        }
    }
    
    function toggleTheatermode() {
        var cover = document.querySelector("#cover");
        if (!isCover) {
            cover.style.display = "block";
            isCover = true;
        } else if (isCover) {
            cover.style.display = "none";
            isCover = false;
        }
    }
                               
    function onSeekRelease() {
        if (!videoWasPaused) {
            video.play();
        }
        blockSeek = false;
    }
    
    function setVolumeBar(volume) {
        var volumeBars = [];
        _.each(controlbars, function(controlbar) {
            volumeBars.push(controlbar.querySelector(".volume"));
        });
        _.each(volumeBars, function(volumeBar) {
            volumeBar.value = volume;
        });
    }

    function setVolumeValue(volume) {
        var i,
            volumebar = document.querySelectorAll(".volume");
        video.volume = volume;
        for (i = 0; i < volumebar.length; i++) {
            volumebar[i].value = volume;
        }
        
        
    }
    
    function updateTimeBar(time) {
        var i,
            value,
            seekbars = document.querySelectorAll(".seekbar");
        if (!blockSeek) {
            for (i = 0; i < seekbars.length; i++) {
                value = (100 / video.duration) * time;
                seekbars[i].value = value;
            }
        }
    }

    function onVolumeChange() {
        var i,
            value,
            volumebar = document.querySelectorAll(".volume");
        for (i = 0; i < volumebar.length; i++) {
            volumebar[i].value = video.volume;
        }
    }
    
    function enableDisableSeekBar() {
        var i,
            seekbars = document.querySelectorAll(".seekbar");
        for (i = 0; i < seekbars.length; i++) {
            duration = video.duration;
            if (duration && !isNaN(duration)) {
                seekbars.max = duration;
                seekbars.disabled = false;
            } else {
                seekbars.disabled = true;
            }
        }
    }
    
    function setTab(area) {
        var i, tabcontent, tablinks,
            areaFeature = document.getElementById(area);

        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        areaFeature.style.display = "block";
        areaFeature.className += " active";
    }
 
    function playVideo() {
        var i;
        for (i = 0; i < controlbars.length; i++) {
            controlbars[i].querySelector(".play").className = "play placeholder-item hidden";
            controlbars[i].querySelector(".pause").className = "pause placeholder-item";
        }
        video.play();
        play = true;
    }
    
    function repeatVideo() {
        var i;
        if (video.loop) {
            for (i = 0; i < controlbars.length; i++) {
                controlbars[i].querySelector(".repeat").className = "repeat placeholder-item";
                controlbars[i].querySelector(".repeatActive").className = "repeatActive placeholder-item hidden";
            }
            video.loop = false;
        } else if (!video.loop) {
            for (i = 0; i < controlbars.length; i++) {
                controlbars[i].querySelector(".repeat").className = "repeat placeholder-item hidden";
                controlbars[i].querySelector(".repeatActive").className = "repeatActive placeholder-item";
            }
            video.loop = true;
        }
    }
    
    function setVideoTime(timeValue) {
        if (!blockSeek) {
            blockSeek = true;
            videoWasPaused = video.paused;
            video.pause();
        }
        video.currentTime = timeValue;
    }

    function pauseVideo() {
        var i;
        for (i = 0; i < controlbars.length; i++) {
            controlbars[i].querySelector(".play").className = "play placeholder-item";
            controlbars[i].querySelector(".pause").className = "pause placeholder-item hidden";
        }
        video.pause();
        play = false;
    }
    
    function stopVideo(){
        pauseVideo();
        video.currentTime = 0;
        play = false;
    }
    
    function onFullscreenExit() {
        if(controllBarPositionSelect.value == 1 || controllBarPositionSelect.value == 0){
                
            controlbars[0].style.display = "none";
        } else {
            controlbars[1].style.display = "none";
        }
        videoContainer.style.cursor = "";
        that.notifyAll("changeIsFullscreen", false);
        isFullscreen = false;
    }
    
    function onFullscreenEnter() {
        that.notifyAll("changeIsFullscreen", true);
        isFullscreen = true;
        showControlbar();
    }
      
    function hideControlbar() {
        videoContainer.style.cursor = "none";
        videoContainer.querySelector(".overlay").style.display = "none";
    }
 
    function showControlbar() {
        videoContainer.style.cursor = "";
        videoContainer.querySelector(".overlay").style.display = "flex";
    }
    
    function toggleFullscreen() { 
        var i;
        for (i = 0; i < controlbars.length; i++) {
            if (controlbars[0].classList.contains("overlay")) {
                controlbars[0].style.display = "flex";
            }
        }
        if (video.webkitRequestFullscreen && !isFullscreen) {
            video.parentElement.webkitRequestFullscreen();
            onFullscreenEnter();
        } else {
            document.webkitExitFullscreen();
            onFullscreenExit();
        }
    }

    function hideOptions() {
        if (optionsContainer.style.display === "none") {
            optionsContainer.style.display = "flex";
            playerContainer.style.width = "70%";
            playerContainer.children[0].style.minWidth = "80%";
        } else {
            optionsContainer.style.display = "none";
            playerContainer.style.width = "100%";
            playerContainer.children[0].style.minWidth = "90%";
        }
    }
    
    function readURL(){
        var file = backgroundImageSelect.files[0],
            reader = new FileReader();
        reader.onloadend = function(){
            document.body.style.background = "url(" + reader.result + ") no-repeat center center fixed";
            document.body.style.backgroundSize = "cover";
        };
        if(file){
            reader.readAsDataURL(file);
        }
    }
    
    function changePlayPauseButtonPosition(playPauseButtonPosition) {
        var playPauseButton = document.querySelectorAll("#play-pause-button"),
            createView,
            newInnerHTML,
            hiddenButton,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(playPauseButton[i] == null) {
                if (playPauseButtonPosition !== 0) {
                    createView = _.template(templateButton);
                    switch (buttonStyleSelect.value){
                    case "0":
                        newInnerHTML = createView({"id": "play-pause-button", "classs": "play placeholder-item", "alt": "Play", "src":"img/iconset_black/player_play.svg","order": playPauseButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                    hiddenButton = createView({"id": "play-pause-button", "classs": "pause placeholder-item hidden", "alt": "Pause", "src":"img/iconset_black/player_pause.svg","order": playPauseButtonPosition});
                    controlbars[i].innerHTML += hiddenButton;
                }
            }else{
                if(playPauseButtonPosition != 0){
                    playPauseButton[i].style.order = playPauseButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("play-pause-button"));
                }
            } 
        }
    }
    
    function changeFeaturePlayPauseMenu(playPauseButtonPosition) {
        playPauseSelect[playPauseButtonPosition].selected = true;
    }
    
    function changeSeekBarPosition(seekBarPosition) {
        var seekBar = document.querySelectorAll("#seek-bar"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(seekBar[i] == null){
                if(seekBarPosition != 0){
                    createView = _.template(templateSeekBar);
                    newInnerHTML = createView({"id": "seek-bar","order": seekBarPosition});
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(seekBarPosition != 0){
                    seekBar[i].style.order = seekBarPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("seek-bar"));

                }
            }
        }
    }
    
    function changeFeatureSeekBarMenu(seekbarButtonPosition) {
        seekbarSelect[seekbarButtonPosition].selected = true;
    }
    
    function changeStopButtonPosition(stopButtonPosition) {
        var stopButton = document.querySelectorAll("#stop-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(stopButton[i] == null){
                if(stopButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0": 
                        newInnerHTML = createView({"id": "stop-button","classs": "stop placeholder-item","alt":"Stop","src":"img/iconset_black/player_stop.svg","order": stopButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(stopButtonPosition != 0){
                    stopButton[i].style.order = stopButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("stop-button"));
                }
            }
        }
    }
    
    function changeFeatureStopMenu(stopButtonPosition) {
        stopSelect[stopButtonPosition].selected = true;
    }
    
    function changeVolumePosition(volumeBarPosition) {
        var volumeBar = document.querySelectorAll("#volume-bar"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(volumeBar[i] == null){
                if(volumeBarPosition != 0){
                    createView = _.template(templateVolumeBar);
                    
                    newInnerHTML = createView({"id": "volume-bar","order": volumeBarPosition});
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(volumeBarPosition != 0){
                    volumeBar[i].style.order = volumeBarPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("volume-bar"));
                }
            }
        }
    }
    
    function changeFeatureVolumeMenu(volumeButtonPosition) {
        volumeSelect[volumeButtonPosition].selected = true;
    }
    
    function changeRepeatButtonPosition(repeatButtonPosition) {
        var repeatButton = document.querySelectorAll("#repeat-button"),
            createView,
            newInnerHTML,
            hiddenButton,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(repeatButton[i] == null){
                if(repeatButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0": 
                        newInnerHTML = createView({"id": "repeat-button","classs": "repeat placeholder-item","alt":"Repeat","src":"img/iconset_black/repeat.svg","order": repeatButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                    hiddenButton = createView({"id": "repeat-button","classs": "repeatActive placeholder-item hidden","alt":"Repeat","src":"img/iconset_colorful/repeat_active.svg","order": repeatButtonPosition});
                    controlbars[i].innerHTML += hiddenButton;

                }
            }else{
                if(repeatButtonPosition != 0){
                    repeatButton[i].style.order = repeatButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("repeat-button"));
                }
            }
        }
    }
    
    function changeFeatureRepeatMenu(repeatButtonPosition) {
        repeatSelect[repeatButtonPosition].selected = true;
    }
    
    function changePrevNextButtonsPosition(prevNextButtonsPosition) {
        var prevButton = document.querySelectorAll("#prev-button"),
            nextButton = document.querySelectorAll("#next-button"),
            createView,
            newInnerHTMLPrev,
            newInnerHTMLNext,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(prevButton[i] == null){
                if(prevNextButtonsPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0": 
                        newInnerHTMLPrev = createView({"id": "prev-button","classs": "prev placeholder-item","alt":"Prev","src":"img/iconset_black/player_prev.svg","order": prevNextButtonsPosition});
                        newInnerHTMLNext = createView({"id": "next-button","classs": "next placeholder-item","alt":"Next","src":"img/iconset_black/player_next.svg","order": prevNextButtonsPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTMLPrev +newInnerHTMLNext;
                }
            }else{
                if(prevNextButtonsPosition != 0){
                    prevButton[i].style.order = prevNextButtonsPosition;
                    nextButton[i].style.order = prevNextButtonsPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("prev-button"));
                    controlbars[i].removeChild(controlbars[i].children.namedItem("next-button"));
                }
            }
        }
    }
    
    function changeFeaturePrevNextMenu(prevNextButtonPosition) {
        prevNextSelect[prevNextButtonPosition].selected = true;
    }
    
    function changeFullscreenButtonPosition(fullscreenButtonPosition) {
        var fullscreenButton = document.querySelectorAll("#fullscreen-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(fullscreenButton[i] == null){
                if(fullscreenButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0": 
                        newInnerHTML = createView({"id": "fullscreen-button","classs": "fullscreen placeholder-item","alt":"Fullscreen","src":"img/iconset_black/fullscreen.svg","order": fullscreenButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(fullscreenButtonPosition != 0){
                    fullscreenButton[i].style.order = fullscreenButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("fullscreen-button"));
                }
            }
        }
    }
    
    function changeFeatureFullscreenMenu(fullscreenButtonPosition){
        fullscreenSelect[fullscreenButtonPosition].selected = true;
    }
    
    function changePopOutButtonPosition(popOutButtonPosition) {
        var popOutButton = document.querySelectorAll("#pop-out-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(popOutButton[i] == null){
                if(popOutButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0": 
                        newInnerHTML = createView({"id": "pop-out-button","classs": "popOut placeholder-item hidden","alt":"PopOut","src":"img/iconset_black/popout.svg","order": popOutButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;

                }
            } else {
                if( popOutButtonPosition != 0) {
                    popOutButton[i].style.order = popOutButtonPosition;
                } else {
                    controlbars[i].removeChild(controlbars[i].children.namedItem("pop-out-button"));
                }
            }
        }
    }
    
    function changeFeaturePopOutMenu(popOutButtonPosition) {
        popOutSelect[popOutButtonPosition].selected = true;
    }
    
    function changeTheaterModePosition(theaterModeButtonPosition) {
        var theaterModeButton = document.querySelectorAll("#theater-mode-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(theaterModeButton[i] == null){
                if(theaterModeButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0":
                        newInnerHTML = createView({"id": "theater-mode-button","classs": "theaterMode placeholder-item","alt":"Theatermode","src":"img/iconset_black/lightswitch.svg","order": theaterModeButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(theaterModeButtonPosition != 0){
                    theaterModeButton[i].style.order = theaterModeButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("theater-mode-button"));
                }
            }
        }
    }
    
    function changeFeatureTheaterModeMenu(theaterModeButtonPosition) {
        theaterModeSelect[theaterModeButtonPosition].selected = true;
    }
    
    function changeAddToPlaylistPosition(addToPlaylistButtonPosition) {
        var addToPlaylistButton = document.querySelectorAll("#add-to-playlist-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(addToPlaylistButton[i] == null){
                if(addToPlaylistButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0":
                        newInnerHTML = createView({"id": "add-to-playlist-button","classs": "addToPlaylist placeholder-item","alt":"AddToPlaylist","src":"img/iconset_black/add.svg","order": addToPlaylistButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;
                }
            }else{
                if(addToPlaylistButtonPosition != 0){
                    addToPlaylistButton[i].style.order = addToPlaylistButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("add-to-playlist-button"));
                }
            }
        }
    }
    
    function changeFeatureAddToPlaylistMenu(addToPlaylistButtonPosition) {
        addToPlaylistSelect[addToPlaylistButtonPosition].selected = true;
    }
    
    function changeOptionsButtonPosition(optionsButtonPosition) {
        var optionsButton = document.querySelectorAll("#options-button"),
            createView,
            newInnerHTML,
            i;
        for (i = 0; i < controlbars.length; i++) {
            if(optionsButton[i] == null){
                if(optionsButtonPosition != 0){
                    createView = _.template(templateButton);
                    switch(buttonStyleSelect.value){
                    case "0":
                        newInnerHTML = createView({"id": "options-button","classs": "options placeholder-item","alt":"Play","src":"img/iconset_black/settings.svg","order": optionsButtonPosition});
                        break;
                    default:
                        break;  
                    }
                    controlbars[i].innerHTML += newInnerHTML;

                }
            }else{
                if(optionsButtonPosition != 0){
                    optionsButton[i].style.order = optionsButtonPosition;
                }else{
                    controlbars[i].removeChild(controlbars[i].children.namedItem("options-button"));
                }
            }
        }
    }   
    
    function changeFeatureOptionsFeatureMenu(optionsFeatureButtonPosition) {
        optionsFeatureSelect[optionsFeatureButtonPosition].selected = true;
    }
    
    function changeControllBarPosition(controllBarPosition) {
        var controllBarPos = parseInt(controllBarPosition);
        switch(controllBarPos){
        //oben
        case 0: 
            playerContainer.style.flexDirection = "column-reverse";
            controlbars[0].style.display = "none";
            controlbars[1].style.display = "flex";
            break;
        //unten
        case 1:
            playerContainer.style.flexDirection = "column";
            controlbars[0].style.display = "none";
            controlbars[1].style.display = "flex";
            break;
        //overlay oben
        case 2:
            videoContainer.style.flexDirection = "column";
            controlbars[0].style.display = "flex";
            controlbars[0].style.marginTop = "2vh";
            controlbars[1].style.display = "none";
            
            break;
        //overlay unten
        case 3:
            videoContainer.style.flexDirection = "column-reverse";
            controlbars[0].style.display = "flex";
            controlbars[0].style.marginTop = "-2vh";
            controlbars[1].style.display = "none";
            break;
        default:
        }
    }
    
    function changeFeatureContralBarPositionMenu(controllBarPosition) {
        controllBarPositionSelect[controllBarPosition].selected = true;
    }
    
    function changePlaylistPosition(playlistPosition){
        var playlistPos = parseInt(playlistPosition);
        switch(playlistPos){
        case 0: 
            playlistContainer.style.display = "none";
            break;
        case 1: 
            playlistContainer.style.display = "flex";
            cusp.style.flexDirection = "row";
            break;
        case 2: 
            playlistContainer.style.display = "flex";
            cusp.style.flexDirection = "row-reverse";
            break;
        default: 
            break;
        }
    }
    
    function changePlaylistPositionMenu(playlistPosition){
        playlistSelect[playlistPosition].selected = true;
    }    
    
    function changePlaylistControlbar(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#show-playlist-controlbar-option", ".playlistControlbar");
    }

    function changePlaylistAddToPlaylistButton(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#add-to-playlist-option", "#add-to-playlist-button-pl");
    }

    function changePlaylistAutoPlayButton(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#autoplay-playlist-option", "#autoplay-playlist-button-pl");
    }

    function changePlaylistPrevNextButtons(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#prev-next-option", "#prev-button-pl");
        changePlaylistControlbarOptions(isActivated, "#prev-next-option", "#next-button-pl");
    }

    function changePlaylistRepeatButtons(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#repeat-option", "#repeat-playlist-button-pl");
    }

    function changePlaylistShuffleButton(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#shuffle-option", "#shuffle-button-pl");
    }

    function changePlaylistReverseOrderButton(isActivated) {
        changePlaylistControlbarOptions(isActivated, "#reverse-option", "#reverse-button-pl");
    }

    function changePlaylistControlbarOptions(isActivated, optionsId, playlistId) {
        playlistTab.querySelector(optionsId).checked = isActivated;
        if (isActivated) {
            playlistContainer.querySelector(playlistId).style.display = "";
        }
        else {
            playlistContainer.querySelector(playlistId).style.display = "none";
        }
    }

    function changeHighlightColor(highlightColor) {
        var i;
        for (i = 0; i < controlbars.length; i++) {
            controlbars[i].style.background = "#" + highlightColor;
        }
        playlistControlbar.style.background = "#" + highlightColor;
        
    }
    
    function changeBackgroundColor(backgroundColor) {
        document.body.style.background = "#" + backgroundColor;
    }
    
    function changeButtonColor(buttonColor) {
        var i,
            svg = document.querySelectorAll("svg");
        for (i = 0; i < svg.length; i++) {
            svg[i].style.fill = "#" + buttonColor;
        }
    }
    
    function changeBackgroundImage(backgroundImage){
        readURL();
    }
    
    function changeStyleBackgroundColor(backgroundColor){
        backgroundColorJSElement.value = backgroundColor;
    }
    
    function changeStyleHighlightColor(highlightColor){
        highlightColorJSElement.value = highlightColor;
    }

    function changeControllBarButtonStyle(buttonStyle) {
                
        var controllBarImages = document.querySelectorAll(".controllBarImage"),
            srcControllBar = controllBarImages[0].src;
        
        changeButtonStyleControllBar(controllBarImages,srcControllBar,buttonStyle);
    }
    
    function changeButtonStyleControllBar(controllBarImages, srcControllBar, buttonStyle){
        var i,
            btnStyle;
        
        btnStyle = parseInt(buttonStyle);
        for(i=0;i<controllBarImages.length;i++){
            if(srcControllBar.search("black")!=-1){
                switch(btnStyle){
                case 0:
                    controllBarImages[i].src = controllBarImages[i].src.replace("black","black");break;
                default:
                    break;
                }
            }
        }
    }
    
    function changeStyleButtonStyle(buttonStyle) {
    }
    
    function changeThemeName(themeName) {
        var options = themeSelect.querySelectorAll("option"),
            isNew = true,
            newOption;
        _.each(options, function(option) {
            if (option.value === themeName) {
                isNew = false;
                if (!option.selected) {
                    option.selected = true;
                }
            }
        });
        if (isNew) {
            newOption = document.createElement("option");
            newOption.value = themeName;
            newOption.innerHTML = themeName;
            newOption.selected = true;
            themeSelect.appendChild(newOption);
        }
    }    
    
    function playPauseVideo() {
        if (play == false) {
            playVideo();
            play  =true;
        } else {
            pauseVideo();
            play = false;
        }
    }
                                                  
    function muteUnmuteVideo() {
        if (video.volume != 0) {
            saveVolume = video.volume;
            setVolumeValue(0);
        } else {
            setVolumeValue(saveVolume);
        }
    }
    
    function volumeUp(){
        if(video.volume < 1){
            video.volume += 0.1;
        }
    }
    
    function volumeDown(){
        if(video.volume > 0 ){
            video.volume -= 0.1;
        }
    }
    
    function updateHotkeys(hotkeys) {
        _.each(hotkeys, function(hotkey, hotkeyName) {
            switch (hotkeyName) {
            case "playPause":
                hotkeyTab.querySelector("#playPauseHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "seekForward":
                hotkeyTab.querySelector("#seekForwardHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "seekBackward":
                hotkeyTab.querySelector("#seekBackwardHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "jumpForward":
                hotkeyTab.querySelector("#jumpForwardHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "jumpBackward":
                hotkeyTab.querySelector("#jumpBackwardHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "stop":
                hotkeyTab.querySelector("#stopHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "volumeUp":
                hotkeyTab.querySelector("#volumeUpHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "volumeDown":
                hotkeyTab.querySelector("#volumeDownHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "mute":
                hotkeyTab.querySelector("#muteHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "repeat":
                hotkeyTab.querySelector("#repeatHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "prevTrack":
                hotkeyTab.querySelector("#previousTrackHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "nextTrack":
                hotkeyTab.querySelector("#nextTrackHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "fullscreen":
                hotkeyTab.querySelector("#fullscreenHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "popOut":
                hotkeyTab.querySelector("#popOutHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "addToPlaylist":
                hotkeyTab.querySelector("#addToPlaylistHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "theaterMode":
                hotkeyTab.querySelector("#theaterModeHotkey").innerHTML = hotkey.toUpperCase();
                break;
            case "options":
                hotkeyTab.querySelector("#settingsHotkey").innerHTML = hotkey.toUpperCase();
                break;
            default:
                break;
            }
        });
    }
    
    function disableSelectedPositions() {
        enableAllPositions();
        disablePlaySelected();
        disableStopSelected();
        disableVolumeSelected();
        disableSeekbarSelected();
        disableRepeatSelected();
        disablePrevNextSelected();
        disableFullscreenSelected();
        disblePopOutSelected();
        disableTheaterModeSelected();
        disableAddToPlaylistSelected();
        disableOptionsFeatureSelected();
        
    }   
    
    function disablePlaySelected() {
        if (playPauseSelect.value != 0) {
            stopSelect[playPauseSelect.value].disabled = true;
            volumeSelect[playPauseSelect.value].disabled = true;
            seekbarSelect[playPauseSelect.value].disabled = true;
            repeatSelect[playPauseSelect.value].disabled = true;
            prevNextSelect[playPauseSelect.value].disabled = true;
            fullscreenSelect[playPauseSelect.value].disabled = true;
            popOutSelect[playPauseSelect.value].disabled = true;
            theaterModeSelect[playPauseSelect.value].disabled = true;
            addToPlaylistSelect[playPauseSelect.value].disabled = true;
            optionsFeatureSelect[playPauseSelect.value].disabled = true;
        }
    }
    
    function disableStopSelected() {
        if (stopSelect.value != 0) {
            playPauseSelect[stopSelect.value].disabled = true;
            volumeSelect[stopSelect.value].disabled = true;
            seekbarSelect[stopSelect.value].disabled = true;
            repeatSelect[stopSelect.value].disabled = true;
            prevNextSelect[stopSelect.value].disabled = true;
            fullscreenSelect[stopSelect.value].disabled = true;
            popOutSelect[stopSelect.value].disabled = true;
            theaterModeSelect[stopSelect.value].disabled = true;
            addToPlaylistSelect[stopSelect.value].disabled = true;
            optionsFeatureSelect[stopSelect.value].disabled = true;
        }
    }
    
    function disableVolumeSelected() {
        if (volumeSelect.value != 0) {
            playPauseSelect[volumeSelect.value].disabled = true;
            stopSelect[volumeSelect.value].disabled = true;
            seekbarSelect[volumeSelect.value].disabled = true;
            repeatSelect[volumeSelect.value].disabled = true;
            prevNextSelect[volumeSelect.value].disabled = true;
            fullscreenSelect[volumeSelect.value].disabled = true;
            popOutSelect[volumeSelect.value].disabled = true;
            theaterModeSelect[volumeSelect.value].disabled = true;
            addToPlaylistSelect[volumeSelect.value].disabled = true;
            optionsFeatureSelect[volumeSelect.value].disabled = true;
        }
    }
    
    function disableSeekbarSelected() {
        if (seekbarSelect.value != 0) {
            playPauseSelect[seekbarSelect.value].disabled = true;
            stopSelect[seekbarSelect.value].disabled = true;
            volumeSelect[seekbarSelect.value].disabled = true;
            repeatSelect[seekbarSelect.value].disabled = true;
            prevNextSelect[seekbarSelect.value].disabled = true;
            fullscreenSelect[seekbarSelect.value].disabled = true;
            popOutSelect[seekbarSelect.value].disabled = true;
            theaterModeSelect[seekbarSelect.value].disabled = true;
            addToPlaylistSelect[seekbarSelect.value].disabled = true;
            optionsFeatureSelect[seekbarSelect.value].disabled = true;
        }
    }
    
    function disableRepeatSelected(){
        if(repeatSelect.value != 0){
            playPauseSelect[repeatSelect.value].disabled = true;
            stopSelect[repeatSelect.value].disabled = true;
            volumeSelect[repeatSelect.value].disabled = true;
            seekbarSelect[repeatSelect.value].disabled = true;
            prevNextSelect[repeatSelect.value].disabled = true;
            fullscreenSelect[repeatSelect.value].disabled = true;
            popOutSelect[repeatSelect.value].disabled = true;
            theaterModeSelect[repeatSelect.value].disabled = true;
            addToPlaylistSelect[repeatSelect.value].disabled = true;
            optionsFeatureSelect[repeatSelect.value].disabled = true;
        }
    }
    
    function disablePrevNextSelected() {
        if (prevNextSelect.value != 0) {
            playPauseSelect[prevNextSelect.value].disabled = true;
            stopSelect[prevNextSelect.value].disabled = true;
            volumeSelect[prevNextSelect.value].disabled = true;
            seekbarSelect[prevNextSelect.value].disabled = true;
            repeatSelect[prevNextSelect.value].disabled = true;
            fullscreenSelect[prevNextSelect.value].disabled = true;
            popOutSelect[prevNextSelect.value].disabled = true;
            theaterModeSelect[prevNextSelect.value].disabled = true;
            addToPlaylistSelect[prevNextSelect.value].disabled = true;
            optionsFeatureSelect[prevNextSelect.value].disabled = true;
        }
    }
    
    function disableFullscreenSelected() {
        if (fullscreenSelect.value != 0) {
            playPauseSelect[fullscreenSelect.value].disabled = true;
            stopSelect[fullscreenSelect.value].disabled = true;
            volumeSelect[fullscreenSelect.value].disabled = true;
            seekbarSelect[fullscreenSelect.value].disabled = true;
            repeatSelect[fullscreenSelect.value].disabled = true;
            prevNextSelect[fullscreenSelect.value].disabled = true;
            popOutSelect[fullscreenSelect.value].disabled = true;
            theaterModeSelect[fullscreenSelect.value].disabled = true;
            addToPlaylistSelect[fullscreenSelect.value].disabled = true;
            optionsFeatureSelect[fullscreenSelect.value].disabled = true;
        }
    }
    
    function disblePopOutSelected() {
        if (popOutSelect.value != 0) {
            playPauseSelect[popOutSelect.value].disabled = true;
            stopSelect[popOutSelect.value].disabled = true;
            volumeSelect[popOutSelect.value].disabled = true;
            seekbarSelect[popOutSelect.value].disabled = true;
            repeatSelect[popOutSelect.value].disabled = true;
            prevNextSelect[popOutSelect.value].disabled = true;
            fullscreenSelect[popOutSelect.value].disabled = true;
            theaterModeSelect[popOutSelect.value].disabled = true;
            addToPlaylistSelect[popOutSelect.value].disabled = true;
            optionsFeatureSelect[popOutSelect.value].disabled = true;
        }
    }
    
    function disableTheaterModeSelected() {
        if (theaterModeSelect.value != 0) {
            playPauseSelect[theaterModeSelect.value].disabled = true;
            stopSelect[theaterModeSelect.value].disabled = true;
            volumeSelect[theaterModeSelect.value].disabled = true;
            seekbarSelect[theaterModeSelect.value].disabled = true;
            repeatSelect[theaterModeSelect.value].disabled = true;
            prevNextSelect[theaterModeSelect.value].disabled = true;
            fullscreenSelect[theaterModeSelect.value].disabled = true;
            popOutSelect[theaterModeSelect.value].disabled = true;
            addToPlaylistSelect[theaterModeSelect.value].disabled = true;
            optionsFeatureSelect[theaterModeSelect.value].disabled = true;
        }
    }
    
    function disableAddToPlaylistSelected() {
        if (addToPlaylistSelect.value != 0) {
            playPauseSelect[addToPlaylistSelect.value].disabled = true;
            stopSelect[addToPlaylistSelect.value].disabled = true;
            volumeSelect[addToPlaylistSelect.value].disabled = true;
            seekbarSelect[addToPlaylistSelect.value].disabled = true;
            repeatSelect[addToPlaylistSelect.value].disabled = true;
            prevNextSelect[addToPlaylistSelect.value].disabled = true;
            fullscreenSelect[addToPlaylistSelect.value].disabled = true;
            popOutSelect[addToPlaylistSelect.value].disabled = true;
            theaterModeSelect[addToPlaylistSelect.value].disabled = true;
            optionsFeatureSelect[addToPlaylistSelect.value].disabled = true;
        }
    }
    
    function disableOptionsFeatureSelected() {
        if (optionsFeatureSelect.value != 0) {
            playPauseSelect[optionsFeatureSelect.value].disabled = true;
            stopSelect[optionsFeatureSelect.value].disabled = true;
            volumeSelect[optionsFeatureSelect.value].disabled = true;
            seekbarSelect[optionsFeatureSelect.value].disabled = true;
            repeatSelect[optionsFeatureSelect.value].disabled = true;
            prevNextSelect[optionsFeatureSelect.value].disabled = true;
            fullscreenSelect[optionsFeatureSelect.value].disabled = true;
            popOutSelect[optionsFeatureSelect.value].disabled = true;
            theaterModeSelect[optionsFeatureSelect.value].disabled = true;
            addToPlaylistSelect[optionsFeatureSelect.value].disabled = true;
        }
    }    
    
    function enableAllPositions() {
        var i;
        for(i = 0; i<playPauseSelect.length; i++) {
            playPauseSelect[i].disabled = false;
            stopSelect[i].disabled = false;
            volumeSelect[i].disabled = false;
            seekbarSelect[i].disabled = false;
            repeatSelect[i].disabled = false;
            prevNextSelect[i].disabled = false;
            fullscreenSelect[i].disabled = false;
            popOutSelect[i].disabled = false;
            theaterModeSelect[i].disabled = false;
            addToPlaylistSelect[i].disabled = false;
            optionsFeatureSelect[i].disabled = false;
        }
    }
    
    // playlist funktionen
    function updatePlaylist(playlist) {
        var selectedPosition = findSelectedPosition();
        clearPlaylist();
        _.each(playlist, function(entry) {
            addPlaylistEntry(entry);
        });
        selectPlaylistEntry(selectedPosition);
    }

    function findSelectedPosition() {
        var selectedPosition;
        _.each(playlistEntries.children, function(entry) {
            if (entry.classList.contains("selected")) {
                selectedPosition = parseInt(entry.attributes.position.value);
            }
        });
        return selectedPosition;
    }

    function clearPlaylist() {
        while (playlistEntries.firstChild) {
            playlistEntries.removeChild(playlistEntries.firstChild);
        }
    }

    function addPlaylistEntry(linkData) {
        var entryNode = document.createElement("div"),
            formatedDuration = formatDuration(linkData.data.duration);

        entryNode.innerHTML = templatePlaylistEntry({
            position: playlistEntries.childElementCount,
            link: linkData.link,
            displayPosition: playlistEntries.childElementCount + 1,
            title: linkData.data.title,
            duration: formatedDuration
        });
        playlistEntries.appendChild(entryNode.children[0]);
    }

    function formatDuration(duration) {
        var formatedDuration = "",
            seconds = duration,
            minutes,
            hours;

        if(seconds) {
            hours = Math.floor(seconds / 3600);
            seconds -= (hours * 3600);
            minutes = Math.floor(seconds / 60);
            seconds -= (minutes * 60);
            if(hours > 0) {
                formatedDuration += (hours + ":");
            }
            if(minutes <= 9) {
                minutes = "0" + minutes;
            }
            formatedDuration += (minutes + ":");
            if(seconds <= 9) {
                seconds = "0" + seconds;
            }
            formatedDuration += seconds;
        }
        return formatedDuration;
    }

    function changeVideo(src) {
        video.src = src;
        playVideo();
    }

    function selectPlaylistEntry(position) {
        if((playlistEntries.childElementCount - 1) < position) {
            return;
        }
        if (position === undefined) {
            return;
        }
        unselectAllPlaylistEntries();
        playlistEntries.children[position].classList.add("selected");
    }

    function unselectAllPlaylistEntries() {
        _.each(playlistEntries.children, function(entry) {
            entry.classList.remove("selected");
        });
    }

    function changeRepeatPlaylist(isOnRepeat) {
        var svg = playlistControlbar.querySelector(".repeatPlaylist .controlbarImage");
        if (isOnRepeat) {
            svg.style.fillOpacity = 1;
        }
        else {
            svg.style.fillOpacity = 0.5;
        }
    }

    function changeAutoplayPlaylist(isOnAutoplay) {
        var svg = playlistControlbar.querySelector(".autoplayPlaylist .controlbarImage");
        if (isOnAutoplay) {
            svg.style.fillOpacity = 1;
        }
        else {
            svg.style.fillOpacity = 0.5;
        }
    }

    function closeAllPrompts() {
        var i,
            children = promptContainer.children;
        for (i = 0; i < children.length; i++) {
            if (children[i].id === "close-prompt") {
                continue;
            }
            children[i].style.display = "none";
        }
        promptContainer.style.display = "none";
        promptContainer.querySelector(".inputURL").value = "";
        promptContainer.querySelector(".inputThemeName").value = "";
        promptContainer.querySelector("#hotkeyTaken").style.display = "none";
        promptContainer.querySelector("#themeNameTaken").style.display = "none";
    }

    function promptItem(promptIdentifier) {
        if (promptIdentifier === "") {
            closeAllPrompts();
            return;
        }
        promptContainer.style.display = "flex";
        promptContainer.querySelector(promptIdentifier).style.display  = "flex";

        switch (promptIdentifier) {
        case ".addLink":
            promptContainer.querySelector(".inputURL").focus();
            break;
        case ".saveTheme":
            promptContainer.querySelector(".inputThemeName").focus();
            break;
        case ".changeHotkey":
            promptContainer.querySelector(".changeHotkey").focus();
            break;
        default:
            break;
        }
    }

    function themeTaken(isTaken) {
        var error = promptContainer.querySelector("#themeNameTaken");
        if (isTaken) {
            error.style.display = "block";
        }
        else {
            error.style.display = "none";
        }
    }

    function hotkeyTaken() {
        promptContainer.querySelector("#hotkeyTaken").style.display = "block";
        setTimeout(function() {
            promptContainer.querySelector("#hotkeyTaken").style.display = "none";
        }, 3000);
    }
    
    that.promptItem = promptItem;
    that.themeTaken = themeTaken;
    
    that.updatePlaylist = updatePlaylist;
    that.changeVideo = changeVideo;
    that.selectPlaylistEntry = selectPlaylistEntry;
    that.changeRepeatPlaylist = changeRepeatPlaylist;
    that.changeAutoplayPlaylist = changeAutoplayPlaylist;

    that.enableDisableSeekBar = enableDisableSeekBar;
    that.updateTimeBar = updateTimeBar;
    that.endVideo = endVideo;
    that.onVolumeChange = onVolumeChange;

    that.onFullscreenExit = onFullscreenExit;
    that.playVideo = playVideo;
    that.pauseVideo = pauseVideo;
    that.stopVideo = stopVideo;
    that.hideOptions = hideOptions;
    that.toggleFullscreen = toggleFullscreen;
    that.setVideoTime = setVideoTime;
    that.onSeekRelease = onSeekRelease;
    that.setVolumeBar = setVolumeBar;
    that.setVolumeValue = setVolumeValue;
    that.repeatVideo = repeatVideo;
    that.toggleTheatermode = toggleTheatermode;
    that.hideControlbar = hideControlbar;
    that.showControlbar = showControlbar;
    
    that.changePlayPauseButtonPosition = changePlayPauseButtonPosition;
    that.changeSeekBarPosition = changeSeekBarPosition;
    that.changeStopButtonPosition = changeStopButtonPosition;
    that.changeVolumePosition = changeVolumePosition;
    that.changeRepeatButtonPosition = changeRepeatButtonPosition;
    that.changePrevNextButtonsPosition = changePrevNextButtonsPosition;
    that.changeFullscreenButtonPosition = changeFullscreenButtonPosition;
    that.changePopOutButtonPosition = changePopOutButtonPosition;
    that.changeTheaterModePosition = changeTheaterModePosition;
    that.changeAddToPlaylistPosition = changeAddToPlaylistPosition;
    that.changeOptionsButtonPosition = changeOptionsButtonPosition;
    that.changeControllBarPosition = changeControllBarPosition;
    
    that.changeFeaturePlayPauseMenu = changeFeaturePlayPauseMenu;
    that.changeFeatureVolumeMenu = changeFeatureVolumeMenu;
    that.changeFeatureStopMenu = changeFeatureStopMenu;
    that.changeFeatureSeekBarMenu = changeFeatureSeekBarMenu;
    that.changeFeatureRepeatMenu = changeFeatureRepeatMenu;
    that.changeFeaturePrevNextMenu = changeFeaturePrevNextMenu;
    that.changeFeatureFullscreenMenu = changeFeatureFullscreenMenu;
    that.changeFeaturePopOutMenu = changeFeaturePopOutMenu;
    that.changeFeatureTheaterModeMenu = changeFeatureTheaterModeMenu;
    that.changeFeatureAddToPlaylistMenu = changeFeatureAddToPlaylistMenu;
    that.changeFeatureOptionsFeatureMenu = changeFeatureOptionsFeatureMenu;
    that.changeFeatureContralBarPositionMenu = changeFeatureContralBarPositionMenu;
    
    that.disableSelectedPositions =  disableSelectedPositions;
    
    that.changeBackgroundColor = changeBackgroundColor;
    that.changeButtonColor = changeButtonColor;
    that.changeHighlightColor = changeHighlightColor;
    that.changeBackgroundImage = changeBackgroundImage;
    that.changeStyleBackgroundColor = changeStyleBackgroundColor;
    that.changeStyleHighlightColor = changeStyleHighlightColor;
    that.changeStyleButtonStyle = changeStyleButtonStyle;
    that.changeControllBarButtonStyle = changeControllBarButtonStyle;
    that.changeThemeName = changeThemeName;
    
    that.updateHotkeys = updateHotkeys;
    that.hotkeyTaken = hotkeyTaken;
                                                
    that.changePlaylistPosition = changePlaylistPosition;
    that.changePlaylistPositionMenu = changePlaylistPositionMenu;
    that.changePlaylistControlbar = changePlaylistControlbar;
    that.changePlaylistAddToPlaylistButton = changePlaylistAddToPlaylistButton;
    that.changePlaylistAutoPlayButton = changePlaylistAutoPlayButton;
    that.changePlaylistPrevNextButtons = changePlaylistPrevNextButtons;
    that.changePlaylistRepeatButtons = changePlaylistRepeatButtons;
    that.changePlaylistShuffleButton = changePlaylistShuffleButton;
    that.changePlaylistReverseOrderButton = changePlaylistReverseOrderButton;
    
    that.setTab = setTab;
    return that;
};
